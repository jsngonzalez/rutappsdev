import UIKit
import Foundation

enum ModalActionType{
    case alert
    case confirm
    case textview
}

typealias ModalActionHandler = () -> Void
typealias ModalActionTextViewHandler = (String) -> Void

class Modal: NSObject {
    
    class func showAlert(_ mensaje:String = "", type: ModalActionType = .alert,textAceptar:String = "", textCancelar:String = "", handlerOK: ModalActionHandler? = nil, handlerError: ModalActionHandler? = nil) {
        let mainstoryboard = UIStoryboard (name: "Modal", bundle: Bundle.main)
    
        var nombreAlerta = "alerta"
        if type == .confirm {
            nombreAlerta = "confirmar"
        }else if type == .textview {
            nombreAlerta = "textView"
        }
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: nombreAlerta) as! ModalViewController
        
        view.titulo = mensaje
        view.textAceptar = textAceptar
        view.textCancelar = textCancelar
        view.type = type
        view.callbackOK = handlerOK
        view.callbackError = handlerError
        
        view.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        view.providesPresentationContextTransitionStyle = true
        view.definesPresentationContext = true
        view.modalPresentationStyle = .overCurrentContext
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(view, animated: true, completion: nil)
        }
        
    }
    
    
    
    class func textView(_ titulo:String = "", textAceptar:String = "Guardar", textCancelar:String = "Cancelar", handlerOK: ModalActionTextViewHandler? = nil, handlerError: ModalActionHandler? = nil) {
        
        let nombreAlerta = "textView"
        let mainstoryboard = UIStoryboard (name: "Modal", bundle: Bundle.main)
        let view = mainstoryboard.instantiateViewController(withIdentifier: nombreAlerta) as! ModalViewController
        
        view.titulo = titulo
        view.textAceptar = textAceptar
        view.textCancelar = textCancelar
        view.type = .textview
        view.callbackOKTexView = handlerOK
        view.callbackError = handlerError
        
        view.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        view.providesPresentationContextTransitionStyle = true
        view.definesPresentationContext = true
        view.modalPresentationStyle = .overCurrentContext
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(view, animated: true, completion: nil)
        }
        
    }
    
    
    
}

