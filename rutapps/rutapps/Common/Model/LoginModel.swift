//
//  LoginModel.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 7/30/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginModel: Mappable {
    var usuario = UsuarioModel(JSON: [:])!
    var rutas = [RutaModel]()
    var placas = [PlacaModel]()
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        usuario <- map["usuario"]
        rutas <- map["rutas"]
        placas <- map["vehiculos"]
    }
}
