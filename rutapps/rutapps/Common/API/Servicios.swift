//
//  Servicios.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 7/15/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import Alamofire
import ZVProgressHUD
import SwiftyUserDefaults
import ObjectMapper
import Crashlytics

struct API {
    static let actividades = "https://api.rutapps.co/movil/v3/actividades.json"
    static let mensajes = "https://api.rutapps.co/movil/v3/mensajes.json"
    static let login = "https://api.rutapps.co/movil/v3/login.json"
    static let rutas = "https://api.rutapps.co/movil/v3/rutas.json"
    static let alertas = "https://api.rutapps.co/movil/v3/alertas.json"
    static let inspecciones = "https://api.rutapps.co/movil/v3/inspecciones.json?idVehiculo="
    static let documentos = "https://api.rutapps.co/movil/v3/documentos.json"
}

class Connectivity {
    class var isConnected:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class Servicios: NSObject {
    
    private var finalCallback: ([String:Any]) -> Void = {response in}
    
    public func post(_ url:String, showLoading:Bool = false, parameters:RequestModel, callback:@escaping ([String:Any]) -> Void ){
        
        
        finalCallback=callback
        
        if Connectivity.isConnected {
            
            
            if showLoading {
                ZVProgressHUD.setMaskType(.black)
                ZVProgressHUD.show()
            }
            
            
            let headers = getHeaders()
            
            print("*REQUEST POST***************************")
            print(url)
            print(parameters.toJSONString(prettyPrint: true)!)
            Alamofire.request(url, method: .post, parameters: parameters.toJSON(), encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if showLoading {
                    ZVProgressHUD.dismiss()
                }
                
                if response.result.isSuccess {
                    
                    if !(response.result.value is NSNull) && (response.result.value != nil) {
                        let JSON = response.result.value
                        if JSON is [String:Any] {
                            print((JSON as! [String:Any]).toJson())
                            self.finalCallback( (JSON as! [String:Any]) )
                        }else{
                            self.finalCallback(["error":1,"response": "Error en la comunicación con el servidor" ])
                        }
                    }else{
                        self.finalCallback(["error":1,"response":"Error al consultar la información."])
                    }
                    
                }else{
                    self.finalCallback(["error":1,"response":response.result.error?.localizedDescription ?? "Error desconocido."])
                }
            }
        }else{
            self.finalCallback(["error":1,"response":"No tiene conexión a internet."])
        }
        
    }
    
    public func get(_ url:String, showLoading:Bool = false, callback:@escaping ([String:Any]) -> Void ){
        
        
        finalCallback=callback
        
        if Connectivity.isConnected {
            
            if showLoading {
                ZVProgressHUD.setMaskType(.black)
                ZVProgressHUD.show()
            }
            
            
            let headers = getHeaders()
            
            
            print("*REQUEST GET***************************")
            print(url)
            print(headers.toJson())
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers ).responseJSON { response in
                
                if showLoading {
                    ZVProgressHUD.dismiss()
                }
                
                if response.result.isSuccess {
                    
                    if !(response.result.value is NSNull) && (response.result.value != nil) {
                        let JSON = response.result.value
                        if JSON is [String:Any] {
                            print((JSON as! [String:Any]).toJson())
                            self.finalCallback( (JSON as! [String:Any]) )
                        }else{
                            self.finalCallback(["error":1,"response": "Error en la comunicación con el servidor" ])
                        }
                    }else{
                        self.finalCallback(["error":1,"response":"Error al consultar la información."])
                    }
                    
                }else{
                    self.finalCallback(["error":1,"response":response.result.error?.localizedDescription ?? "Error desconocido."])
                }
            }
        }else{
            self.finalCallback(["error":1,"response":"No tiene conexión a internet."])
        }
    }
    
    func getHeaders() -> HTTPHeaders{
        var key=""
        if Defaults.hasKey(.usuario) {
            let usuario = UsuarioModel(JSON: (Defaults[.usuario] ) )!
            key = usuario.token
        }
        
        
        var playerID = ""
        if Defaults.hasKey(.playerID) {
            playerID = Defaults[.playerID]
        }
        
        var appVersionString = ""
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String {
            appVersionString = version
        }
        
        
        print("token:"+key)
        
        let headers: HTTPHeaders = [
            "X-API-KEY": key,
            "X-PLAYER-ID": playerID,
            "SO": "IOS",
            "version_SO": UIDevice.current.systemVersion,
            "version_APP": appVersionString,
            "Accept": "application/json"
        ]
        
        return headers
    }
    
}
