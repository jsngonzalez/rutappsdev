//  
//  CompartirServicioViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 8/6/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

enum CompartirType{
    case alert
    case confirm
}

class CompartirServicioViewController: UIViewController {
    
    
    @IBOutlet var viewContent: UIView!
    @IBOutlet var btnCompartir: UIButton!
    @IBOutlet var lblCodigo: UILabel!
    
    var codigo = ""
    var link = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initView()
        agregarBlur()
    }
    
    func agregarBlur(){
        
        self.viewContent.sombra()
        self.view.insertSubview(blureffect(), at: 0)
    }
    
    func initView() {
        
        viewContent.frame = CGRect(x: viewContent.frame.origin.x, y: screen_height, width: viewContent.frame.size.width , height: viewContent.frame.size.height)
        viewContent.backgroundColor = .white

        lblCodigo.text = codigo
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let heigth = viewContent.frame.size.height + 20
        let destination = screen_height - heigth
        
        UIView.animate(withDuration: 0.2) {
            var frame = self.viewContent.frame
            frame.origin.y = destination
            self.viewContent.frame = frame
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.2) {
            var frame = self.viewContent.frame
            frame.origin.y = self.screen_height
            self.viewContent.frame = frame
        }
        
        super.viewWillDisappear(animated)
    }
    
    @objc func cerrar(){
        
        view.endEditing(true)
        dismiss(animated: true)
        
    }
    
    @IBAction func cerrarAlerta(_ sender: Any) {
        cerrar()
    }
    
    @IBAction func compartir(_ sender: Any) {
        
       shared(link: link)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
