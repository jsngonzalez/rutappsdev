//
//  configuracionServicioViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 22/11/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import PMSuperButton
import ZVProgressHUD

class configuracionServicioViewController: UIViewController {

    @IBOutlet var lblNombreRuta: UILabel!
    @IBOutlet var btnAtras: UIButton!
    @IBOutlet var table: UITableView!
    @IBOutlet var btnNuevo: PMSuperButton!
    @IBOutlet var btnAgregarActividad: PMSuperButton!
    
    var container = [ActividadModel]()
    
    
    struct CellType {
        static let blanco = "CellBlank"
        static let actividad = "CellActividad"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    func initView() {
        
        lblNombreRuta.text = ruta_activa.nombre
        
        btnNuevo.touchUpInside{
            self.iniciarServicio()
        }
        
//        btnAgregarActividad.touchUpInside{
//            self.agregarActividad()
//        }
        
        btnAtras.addAction {
            self.atras()
        }
        
        if appDelagete.solicitarGPS() {
            showAlert(.txt_error_gps, textAceptar: "Habilitar", textCancelar: "Cancelar", callback: {
                
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                
            }, callbackCancel: nil)
        }
        
        container = ruta_activa.actividades
    }
    
    func iniciarServicio(){
        
        showAlert("¿Seguro que deseas iniciar el servicio \(ruta_activa.nombre.uppercased()) ?",textAceptar:"SÍ",textCancelar:"NO", callback: {
            ZVProgressHUD.setMaskType(.black)
            ZVProgressHUD.show()
            
            let actividades = self.container.filter { (item) -> Bool in
                return item.activo
            }
            
            FirebaseDB.nuevoServicio(self.ruta_activa, actividades: actividades) { (ok, response) in
                ZVProgressHUD.dismiss()
                if ok {
                    let controller = UINavigationController.instance(fromAppStoryboard: .Servicio)
                    Routing.show(controller)
                }else{
                    self.showAlert(response)
                }
            }
        }, callbackCancel: nil);
        
       
    }
    
    func agregarActividad () {
        let controller = BuscarActividadViewController.instance(fromAppStoryboard: .BuscarActividad)
        Routing.popup(controller)
    }
    
    func atras(){
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension configuracionServicioViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CellType.actividad, for: indexPath) as! CellGenerica

        
        cell.label1.text = item.nombre
        
        if item.activo {
            cell.img1.backgroundColor = #colorLiteral(red: 0, green: 0.8613275886, blue: 0.3820592165, alpha: 1)
            cell.img1.image = UIImage.init(named: "ic_check_white_36pt")
        }else{
            cell.img1.backgroundColor = #colorLiteral(red: 1, green: 0.6, blue: 0.1411764706, alpha: 1)
            cell.img1.image = UIImage.init(named: "baseline_remove_white_36dp")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 107
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        container = container.map { (item) -> ActividadModel in
            item.activo = item.idActividad == container[indexPath.row].idActividad ? !item.activo : item.activo
            return item
        }
        
        table.beginUpdates()
        table.reloadRows(at: [IndexPath(item: indexPath.row, section: 0)], with: .automatic)
        table.endUpdates()
        table.setContentOffset(table.contentOffset, animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: screen_width, height: 90))
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: screen_width, height: 120))
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    
}
