//
//  ActividadModel.swift
//  rutapps
//
//  Created by MacBook on 8/3/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class ActividadModel: Mappable {
    
    var idActividad = ""
    var nombre = ""
    var descripcion = ""
    var estado = ""
    var codigo = ""
    var link = ""
    var orden = ""
    var atajos = [EstadoModel]()
    var lng = 0.0
    var lat = 0.0
    var activo = false
    
    
    var error = 1
    var response : Any?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idActividad <- map["idActividad"]
        nombre <- map["nombre"]
        descripcion <- map["descripcion"]
        estado <- map["estado"]
        codigo <- map["codigo"]
        link <- map["link"]
        atajos <- map["atajos"]
        lng <- map["lng"]
        lat <- map["lat"]
        activo <- map["activo"]
        
        
        error <- map["error"]
        response <- map["response"]
    }
    
}
