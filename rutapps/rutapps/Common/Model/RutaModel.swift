//
//  VehiculosModel.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class RutaModel: Mappable {
    
    var idRuta = ""
    var nombre = ""
    var codigo = ""
    var descripcion = ""
    var vehiculo = ""
    var empresa = ""
    var token = ""
    var estado = ""
    var link = ""
    var urlMapa = ""
    var idServicio = ""
    var actividades = [ActividadModel]()
    var activo = 0
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map)
    {
        idRuta <- map["idRuta"]
        nombre <- map["nombre"]
        codigo <- map["codigo"]
        descripcion <- map["descripcion"]
        vehiculo <- map["vehiculo"]
        empresa <- map["empresa"]
        token <- map["token"]
        estado <- map["estado"]
        link <- map["link"]
        urlMapa <- map["urlMapa"]
        idServicio <- map["idServicio"]
        activo <- map["activo"]
        actividades <- map["actividades"]
    }
    
}
