//
//  Dictionary+extension.swift
//  rutapps
//
//  Created by IMAC on 14/09/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit

extension Dictionary {
    
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func toJson() -> String {
        return json
    }
    
    func toJsonUrlEncode() -> String {
        let urlEncode = json.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return urlEncode!
    }
    
}
