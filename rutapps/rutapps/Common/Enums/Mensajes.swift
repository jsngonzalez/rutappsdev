//
//  Mensajes.swift
//  rutapps
//
//  Created by IMAC on 1/08/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//
import UIKit

extension String {
    
    static let txt_error_login = "Algo salió mal, intentalo de nuevo."
    static let txt_img_cargando = "https://firebasestorage.googleapis.com/v0/b/rutapps-183a3.appspot.com/o/app%2Floading.json?alt=media&token=984e020b-dea8-45ce-bf69-62b59a5a2115"
    static let txt_input_adjuntar_archivo = "Aquí puedes dejar un mensaje..."
    static let txt_error_gps = "Es importante habilitar el uso del GPS siempre para tener una correcta ubicación mientras realizas el recorrido"
    
    func tag(_ tag:String) -> String{
        return ""
    }
    
}
