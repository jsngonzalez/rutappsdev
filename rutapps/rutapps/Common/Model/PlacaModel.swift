//
//  PlacaModel.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 22/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class PlacaModel: Mappable {
    
    var idVehiculo = ""
    var placa = ""
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map)
    {
        idVehiculo <- map["idVehiculo"]
        placa <- map["placa"]
    }
    
}
