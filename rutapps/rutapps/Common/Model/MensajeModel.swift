//
//  MensajeModel.swift
//  rutappsusuario
//
//  Created by MacBook on 10/29/19.
//  Copyright © 2019 devops. All rights reserved.
//

import UIKit
import ObjectMapper

class MensajeModel: Mappable {
    
    var mensaje = ""
    var ruta = RutaModel(JSON:[:])!
    var actividad = ActividadModel(JSON:[:])!
    var reg = ""
    var idRuta = 0
    var idActividad = 0
    var idMensaje = 0
    
    var error = 1
    var response : Any?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        mensaje <- map["mensaje"]
        ruta <- map["ruta"]
        actividad <- map["actividad"]
        reg <- map["reg"]
        idRuta <- map["idRuta"]
        idActividad <- map["idActividad"]
        idMensaje <- map["idMensaje"]
        
        
        error <- map["error"]
        response <- map["response"]
    }
}
