//
//  UIImageView.swift
//  ideas-claro
//
//  Created by IMAC on 11/9/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//
import Foundation
import UIKit
import Alamofire
import AlamofireImage
import Lottie


@IBDesignable
open class SuperImageView: UIImageView {
    
    @IBInspectable open var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable open var borderColor: UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0){
        didSet{
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = 1
        }
    }
    
}

extension UIImageView{
    
    func loadImage(_ url:String){
        
        let animationView = AnimationView(name: "loader")
        self.addSubview(animationView)
        
        let with = ((self.frame.size.width > self.frame.size.height) ? self.frame.size.height : self.frame.size.width)
        let heigth = with * 200 / 200
        
        animationView.frame.size.width=with
        animationView.frame.size.height=heigth
        animationView.frame.origin.x = (self.frame.size.width / 2) - (animationView.frame.size.width / 2)
        animationView.frame.origin.y = (self.frame.size.height / 2) - (animationView.frame.size.height / 2)
        animationView.loopMode = .loop
        animationView.play()
        
        if url != "" {
            let urlRequest = URLRequest(url: URL(string: url)!)
            Alamofire.request(urlRequest).responseImage { response in
                if let image = response.result.value {
                    animationView.removeFromSuperview()
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.layer.opacity = 0
                    }, completion: { finished in
                        self.image = image
                        UIView.animate(withDuration: 0.3, animations: {
                            self.layer.opacity = 1
                        })
                    })
                }
            }
        }
        
    }
    
    
    func loadImageNoAmination(_ url:String){
        
        if url != "" {
            let urlRequest = URLRequest(url: URL(string: url)!)
            Alamofire.request(urlRequest).responseImage { response in
                if let image = response.result.value {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.layer.opacity = 0
                    }, completion: { finished in
                        self.image = image
                        UIView.animate(withDuration: 0.3, animations: {
                            self.layer.opacity = 1
                        })
                    })
                }
            }
        }
        
    }
    
    
}
