//
//  ArchivoViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 29/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ZVProgressHUD
import Firebase
import SwiftyUserDefaults
import ObjectMapper
import Actions
import DatePickerDialog

class ArchivoViewController: UIViewController {

    @IBOutlet var table: UITableView!
    @IBOutlet var btnSalir: UIButton!
    @IBOutlet var lblTitulo: UILabel!
    
    var container = [(cell:String,data:[String:Any])]()
    let documento = DocumentosModel(JSON: Defaults[.documento_activo])!
    var archivo = ArchivoModel(JSON: [:])!
    
    var fecha = ""
    var fotos = [FotoModel]()
    
    
    
    struct CellType {
        static let blanco = "CellBlank"
        static let fecha = "CellFecha"
        static let observaciones = "CellObservaciones"
        static let foto = "CellFoto"
        static let nuevaFoto = "CellNuevaFoto"
        static let boton = "CellBoton"
    }
    
    struct txt {
        static let fecha = "Fecha de vencimiento"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView(){
        
        archivo = documento.archivo
        archivo.idTipo = documento.idTipo
        
        lblTitulo.text = documento.nombre
        fecha = txt.fecha
        
        btnSalir.addAction {
            self.navigationController?.popViewController(animated: true)
        }
        
        //documentos = Mapper<DocumentosModel>().mapArray(JSONObject: Defaults[.documentos] )!
        initTable()
    }
    
    func initTable(){
        
        container = [(cell:String,data:[String:Any])]()
        container.append((cell: CellType.blanco, data: [:] ))
        
        if documento.solicitar_fecha == 1 {
            container.append((cell: CellType.fecha, data: [:] ))
        }
        
        container.append((cell: CellType.observaciones, data: [:] ))
        
        for item in fotos {
            container.append((cell: CellType.foto, data: ["id":item.id] ))
        }
        
        container.append((cell: CellType.nuevaFoto, data: [:] ))
        container.append((cell: CellType.boton, data: [:] ))
        
//        for item in documentos {
//            container.append((cell: CellType.titulo, data: item.toJSON() ))
//        }
        
        table.reloadData()
    }
    
    func seleccionarFecha(){
        DatePickerDialog(locale: Locale(identifier: "es_CO")).show("Fecha de vencimiento", doneButtonTitle: "OK", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatterDia = DateFormatter()
                formatterDia.dateFormat = "dd"
                
                let formatterMes = DateFormatter()
                formatterMes.dateFormat = "MM"
                
                let formatterAnio = DateFormatter()
                formatterAnio.dateFormat = "yyyy"
                
                self.archivo.dia = Int(formatterDia.string(from: dt))!
                self.archivo.mes = Int(formatterMes.string(from: dt))!
                self.archivo.anio = Int(formatterAnio.string(from: dt))!
                
                self.fecha = "\(self.archivo.dia) de \( self.nombreMes(self.archivo.mes) ) del \(self.archivo.anio)"
                
                self.table.reloadData()
            }
        }
    }
    
    func subirFoto(){
        let path = "archivos/empresa_\(usuario.idEmpresa)/documentos/"
        
        subirFotoFirebase(path: path) { (url, image) in
            print(url)
            let foto = FotoModel(JSON:[:])!
            foto.url = url
            foto.image = image
            foto.id = UUID().uuidString
            
            self.fotos.append( foto )
            self.initTable()
        }
    }
    
    func eliminarFoto(_ id:String){
        let fotosFilter = fotos.filter { (item) -> Bool in
            return item.id != id
        }
        
        fotos = fotosFilter
        initTable()
    }
    
    func guardar(){
        
        let listaFotos = fotos.map { (item) -> String in
            return item.url
        }
        
        archivo.archivos = arrayToJSON(listaFotos)
        
        DocumentosManager.guardar(archivo) { (ok, res) in
            if ok {
                self.showAlert("La información se envió exitosamente", callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }else{
                self.showAlert(res)
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ArchivoViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! CellGenerica
        
        if (item.cell == CellType.blanco){
            
        }else if (item.cell == CellType.fecha){
            
            cell.btn1.addAction {
                self.seleccionarFecha()
            }
            cell.txt1.text = fecha
            
        }else if (item.cell == CellType.observaciones){
            
            cell.txt1.throttle(.editingChanged, interval: 0.2) {
                self.archivo.mensaje = cell.txt1.text!
            }
            
        }else if (item.cell == CellType.foto){
            
            let query = FotoModel(JSON:item.data)!
            let foto = fotos.filter { (item) -> Bool in
                return item.id == query.id
            }
            
            cell.img1.image = foto.first!.image
            
            cell.btnPM1.addAction {
                self.eliminarFoto(query.id)
            }
            
        }else if (item.cell == CellType.nuevaFoto){
            
            cell.btnPM1.addAction {
                self.subirFoto()
            }
            
        }else if (item.cell == CellType.boton){
            
            cell.btnPM1.addAction {
                self.guardar()
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = container[indexPath.row]
        
        
        if (item.cell == CellType.blanco){
            return 30
        }else if (item.cell == CellType.fecha){
            return 86
        }else if (item.cell == CellType.observaciones){
            return 86
        }else if (item.cell == CellType.foto){
            return 218
        }else if (item.cell == CellType.nuevaFoto){
            return 64
        }else if (item.cell == CellType.boton){
            return 64
        }else{
            return 0
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
