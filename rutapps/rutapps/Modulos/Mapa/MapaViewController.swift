//
//  MapaViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 15/11/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import WebKit

class MapaViewController: UIViewController {

    @IBOutlet var viewContainer: SuperView!
    @IBOutlet var webView: WKWebView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    var url = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewContainer.bounceIn(from: .bottom)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.viewContainer.frame.origin.y = self.screen_height
        }
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func initView(){
        
        self.view.insertSubview(blureffect(), at: 0)
        
        //viewContainer.frame.origin.y = screen_height
        viewContainer.add(gesture: .multiSwipe(direction: .down, fingers: 1)) { (view: UIView) in
            self.cerrar()
        }
        
        print("urlString: \(url)")
        
        if (url != ""){
            let link = URL(string:url)!
            let request = URLRequest(url: link)
            
            webView.navigationDelegate = self
            webView.load(request)
            webView.allowsBackForwardNavigationGestures = true
        }else{
            showAlert("No se puede mostrar el mapa en este momento. intentalo más tarde"){
                self.cerrar()
            }
        }
        
        
        
    }
    
    @IBAction func cerrarmenu(_ sender: Any) {
        cerrar()
    }
    
    
    @IBAction func volver(_ sender: Any) {
        cerrar()
    }
    
    func cerrar(){
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MapaViewController: WKNavigationDelegate {
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        spinner.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        spinner.stopAnimating()
    }
    
    
}
