//
//  AlertasManager.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 8/20/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults

class AlertasManager: NSObject {
    
    class func refrescar(callback:@escaping (Bool,String) -> Void = {(ok,error) in} ){
        
        Servicios().get(API.alertas, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    
    
    class func validar(_ json:[String:Any]) -> Bool{
        
        let response = MapResponse.toArray(json, with: AlertaModel.self)
        if response.error == 0, let response = response.response {
            Defaults[.alertas] = response.toJSON()
            return true
        }else{
            return false
        }
        
    }
    
}
