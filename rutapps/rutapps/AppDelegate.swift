//
//  AppDelegate.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 22/12/17.
//  Copyright © 2017 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import Firebase
import SwiftyUserDefaults
import CoreLocation
import OneSignal
import IQKeyboardManagerSwift
import YPImagePicker
import SwiftLocation
import Notifwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locationManager : LocationRequest?
    var backgroundLlocationManager : LocationRequest?
    let notifwift = Notifwift()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        /*
        for family in UIFont.familyNames {
            print(family)
            for name in UIFont.fontNames(forFamilyName: family){
                print(name)
            }
        }*/
        
        /*
        Defaults.remove(.key_servicio)
        Defaults.remove(.usuario)
        Defaults.remove(.empresa)*/
        
        var config = YPImagePickerConfiguration()
        
        config.wordings.libraryTitle = "Galería"
        config.wordings.cameraTitle = "Camara"
        config.wordings.next = "OK"
        config.wordings.cancel = "Cancelar"
        
        config.colors.tintColor = .green // Right bar buttons (actions)
        
        
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = false
        config.showsPhotoFilters = false
        config.shouldSaveNewPicturesToAlbum = true
        config.startOnScreen = .photo
        config.screens = [.library, .photo]
        config.showsCrop = .none
        config.targetImageSize = .original
        config.overlayView = UIView()
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedTextColour = #colorLiteral(red: 0.1490196078, green: 0.1490196078, blue: 0.1490196078, alpha: 1)
        config.bottomMenuItemUnSelectedTextColour = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        config.library.mediaType = .photo
        
        YPImagePickerConfiguration.shared = config

        
        FirebaseApp.configure()
        Auth.auth().languageCode = "es"
        Database.database().isPersistenceEnabled = true
        
        
        IQKeyboardManager.shared.enable = true
        
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.titleTextAttributes = [
            .font: UIFont(name: fontConfig.OpenSans.Bold, size: 20)!,
            .foregroundColor:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ]
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([
            .foregroundColor: UIColor.clear
            ], for: .normal)
        
        
        if let _ = launchOptions?[UIApplication.LaunchOptionsKey.location] {
            if Defaults.hasKey(.ruta_activa) {
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.GPSBackground()
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppAlerts : false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "54007a58-ed5d-4ea3-84fd-c19038a9bec4",
                                        handleNotificationAction: { result in
                                            
                                            let payload: OSNotificationPayload = result!.notification.payload
                                            print(payload.additionalData ?? "No hay additionalData")
                                            
                                            if let data = payload.additionalData as? [String : Any] {
                                                
                                                Defaults[.pushNoti] = data
                                                
                                                let state = UIApplication.shared.applicationState
                                                if state == .background  || state == .active {
                                                    Notifwift.post(.pushNoti)
                                                }
                                                
                                            }
                                            
                                            
                                            
                                        },
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        
        notifwift.observe(.pushNoti) { notificacion in
            self.verificarNotificacion()
        }
        
        if Defaults.hasKey(.ruta_activa) && Defaults.hasKey(.usuario){
            let controller = CargandoServicioActivoViewController.instance(fromAppStoryboard: .CargandoServicioActivo)
            Routing.show(controller)
            
        }else{
            let controller = LoginViewController.instance(fromAppStoryboard: .Login)
            Routing.show(controller)
        }
        
        #if compiler(>=5.1)
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        #endif
        
        return true
    }
    
    func GPSBackground(){
        backgroundLlocationManager = LocationManager.shared.locateFromGPS(.significant, accuracy: .neighborhood) { result in
            switch result {
            case .failure(let error):
                debugPrint("Received error: \(error)")
            case .success(let location):
                debugPrint("Location received: \(location)")
                FirebaseService.nuevoPunto(location)
            }
        }
    }
    
    
    func solicitarGPS() -> Bool{
        
        var solicitarPermiso = false
        
        if (!CLLocationManager.locationServicesEnabled()) {
            solicitarPermiso = true
        }
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            LocationManager.shared.requireUserAuthorization(.always)
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            //initLocation()
            solicitarPermiso = true
            break
        case .authorizedAlways:
            // If always authorized
            //initLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            solicitarPermiso = true
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            solicitarPermiso = true
            break
        default:
            break
        }
        
        return solicitarPermiso
    }
    
    func verificarNotificacion() {
        
        if Defaults.hasKey(.pushNoti) && Defaults.hasKey(.usuario) {
            let noti  = PushModel(JSON: (Defaults[.pushNoti]) )!
            if noti.tipo == tipoPush.servicio {
                
            }
        }
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

