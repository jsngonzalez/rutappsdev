//
//  RequestModel.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class RequestModel: Mappable {
    var data:[String:Any] = [:]
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}
