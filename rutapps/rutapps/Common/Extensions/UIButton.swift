//
//  UIButton.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 12/29/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import PMSuperButton

extension UIButton {
    
    func duplicatePMSuperButton() -> UIButton? {
        let archivedButton = NSKeyedArchiver.archivedData(withRootObject: self)
        guard let buttonDuplicate = NSKeyedUnarchiver.unarchiveObject(with: archivedButton) as? PMSuperButton else { return nil }
        return buttonDuplicate
    }
}
