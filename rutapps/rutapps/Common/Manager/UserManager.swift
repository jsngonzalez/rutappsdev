//
//  UserManager.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 8/20/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults
import Crashlytics

class UserManager: NSObject {
    
    
    class func login(usuario:String, clave:String, callback:@escaping (Bool,String) -> Void ){
        
        let usuario = UsuarioModel(JSON: ["usuario":usuario,"clave":clave])!
        let req = RequestModel(JSON: ["data":usuario.toJSON()] )!
        
        Crashlytics.sharedInstance().setObjectValue(API.login, forKey: "url")
        Crashlytics.sharedInstance().setObjectValue("login", forKey: "metodo")
        Crashlytics.sharedInstance().setObjectValue(req.toJSONString(), forKey: "request")
        
        Servicios().post(API.login, showLoading: true, parameters:req, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    class func refrescar(callback:@escaping (Bool,String) -> Void ){
        
        
        Crashlytics.sharedInstance().setObjectValue("login_get", forKey: "metodo")
        
        Servicios().get(API.login, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    class func validar(_ json:[String:Any]) -> Bool{
        
        Crashlytics.sharedInstance().setObjectValue(json, forKey: "respuesta_login")
        
        let response = MapResponse.toObject(json, with: LoginModel.self)
        if response.error == 0, let response = response.response {
            Defaults[.rutas] = response.rutas.toJSON()
            Defaults[.usuario] = response.usuario.toJSON()
            Defaults[.placas] = response.placas.toJSON()
            return true
        }else{
            return false
        }
        
    }
    
}
