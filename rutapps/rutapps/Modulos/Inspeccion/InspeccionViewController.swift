//
//  InspeccionViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 20/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ZVProgressHUD
import SwiftyUserDefaults
import ObjectMapper

class InspeccionViewController: UIViewController {
    
    @IBOutlet var table: UITableView!
    @IBOutlet var lblNombre: UILabel!
    @IBOutlet var btnSalir: UIButton!
    
    var container = [(cell:String,data:[String:Any],parent:[String:Any])]()
    
    struct CellType {
        static let blanco = "CellBlank"
        static let inspeccion = "CellInspeccion"
        static let titulo = "CellTitulo"
        static let boton = "CellBoton"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    
    func initView(){
        
        lblNombre.text = "Fecha: \(inspeccion_activa.nombre)"
        
        btnSalir.addAction {
            self.navigationController?.popViewController(animated: true)
        }
        
        initTable()
    }
    
    func initTable(){
        
        container = [(cell:String,data:[String:Any],parent:[String:Any])]()
        container.append((cell: CellType.blanco, data: [:], parent: [:] ))
        
        for form in inspeccion_activa.formulario {
            container.append((cell: CellType.titulo, data: form.toJSON(), parent: [:] ))
            for item in form.inspecciones {
                container.append((cell: CellType.inspeccion, data: item.toJSON(), parent: form.toJSON() ))
            }
        }
        
        container.append((cell: CellType.boton, data: [:], parent: [:] ))
        
        table.reloadData()
    }
    
    func guardarRespuesta(_ opcion:InspeccionModel){
        
        print(index)
        print(opcion.toJSON())
        
        container = container.map({ (cell, data, parent) -> (cell:String,data:[String:Any],parent:[String:Any]) in
            if (cell == CellType.titulo){
                return (cell: cell, data: data, parent: [:] )
            }else if (cell == CellType.inspeccion){
                let op = InspeccionModel(JSON: data)!
                if (op.idInspeccion == opcion.idInspeccion){
                    return (cell: cell, data: opcion.toJSON(), parent: parent )
                }else{
                    return (cell: cell, data: data, parent: parent )
                }
            }else{//blanco
                return (cell: cell, data: [:], parent: [:] )
            }
        })
        
    }
    
    func guardar(){
        
        let res = container.filter { (cell, data, parent) -> Bool in
            return cell == CellType.inspeccion
        }
        
        let listaInspecciones = res.map { (cell, data, parent) -> InspeccionModel in
             let op = InspeccionModel(JSON: data)!
            return op
        }
        
        var listNoCompletadas = [String]()
        let noCompletadas = listaInspecciones.filter { (item) -> Bool in
            
            if item.respuesta == "" {
                listNoCompletadas.append(item.nombre)
                return true
            }else{
                return false
            }
        }
        
        if noCompletadas.count > 0 {
            showAlert("Debe completar todo el formulario, falta por responder: \(listNoCompletadas.joined(separator: ","))")
        }else{
            
            
            InspeccionManager.guardar(listaInspecciones, dia: inspeccion_activa) { (ok, res) in
                if ok {
                    self.showAlert("Se realizó exitosamente la inspección", callback: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.showAlert(res)
                }
            }
        }
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension InspeccionViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! CellGenerica
        
        if (item.cell == CellType.titulo){
            let dia = FormularioModel(JSON: item.data)!
            cell.label1.text = dia.nombre
        }else if (item.cell == CellType.inspeccion){
            let opcion = InspeccionModel(JSON: item.data)!
            cell.label1.text = opcion.nombre
            cell.pintarOpcionesFormulario(opcion , viewController: self)
        }else if (item.cell == CellType.boton){
            cell.btnPM1.addAction {
                self.guardar()
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = container[indexPath.row]
        
        if (item.cell == CellType.titulo){
            return 104
        }else if (item.cell == CellType.inspeccion){
            return 187
        }else if (item.cell == CellType.blanco){
            return 30
        }else if (item.cell == CellType.boton){
            return 97
        }else{
            return 0
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = container[indexPath.row]
        
        if (item.cell == CellType.titulo){
            
        }
    }
    
}
