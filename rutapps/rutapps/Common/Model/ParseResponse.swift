//
//  ParseResponse.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 7/15/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class mapResponse: NSObject {
    
    class func toObject<T: Mappable>(_ object:[String:Any], with: T.Type) -> ResponseObjectModel<T> {
        
        let response = Mapper<ResponseObjectModel<T>>().map(JSON:object)!
        
        return response
        
    }
    
    class func toArray<T: Mappable>(_ object:[String:Any], with: T.Type) -> ResponseArrayModel<T> {
        
        let response = Mapper<ResponseArrayModel<T>>().map(JSON:object)!
        return response
    }
    

}
