//
//  UsuarioModel.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class EmpresaModel: Mappable {
    var nombre = ""
    var rutas = [RutaModel]()
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        nombre <- map["nombre"]
        rutas <- map["rutas"]
    }
}
