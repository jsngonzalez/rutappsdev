//
//  VehiculosViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 24/12/17.
//  Copyright © 2017 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ZVProgressHUD
import Firebase
import SwiftyUserDefaults
import ObjectMapper
import Actions
import SwiftLocation


class RutasViewController: MasterUIViewController {
    
    @IBOutlet var table: UITableView!
    @IBOutlet var btnSalir: UIButton!
    @IBOutlet var btnPerfil: UIButton!
    @IBOutlet var lblVersion: UILabel!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(cargarRutas),for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .gray
        return refreshControl
    }()
    
    var rutas:[RutaModel] = Mapper<RutaModel>().mapArray(JSONObject: Defaults[.rutas] )!
    
    var container = [(cell:String,data:[String:Any])]()
    
    struct CellType {
        static let blanco = "CellBlank"
        static let ruta = "CellRuta"
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        // Do any additional setup after loading the view.
        table.addSubview(refreshControl)
        self.initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.cargarRutas()
    }
    
    func initView() {
        
        
        self.rutas = Mapper<RutaModel>().mapArray(JSONObject: Defaults[.rutas] )!
        self.initTable()
        
        btnSalir.addAction {
            self.navigationController?.popViewController(animated: true)
        }
        
        btnPerfil.addAction {
            //self.salir()
        }
        
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        lblVersion.text = appVersion
        
        if appDelagete.solicitarGPS() {
            showAlert(.txt_error_gps, textAceptar: "Habilitar", textCancelar: "Cancelar", callback: {
                
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                
            }, callbackCancel: nil)
        }
    }
    
    func initTable(){
        
        container = [(cell:String,data:[String:Any])]()
        container.append((cell: CellType.blanco, data: [:] ))
        
        for item in rutas {
            container.append((cell: CellType.ruta, data: item.toJSON() ))
        }
        
        table.reloadData()
    }
    
    func cerrarSesion(){
        Defaults.removeAll()
        let controller = LoginViewController.instance(fromAppStoryboard: .Login)
        Routing.show(controller)
    }
    
    @objc func cargarRutas() {
        
        refreshControl.beginRefreshing()
        
        UserManager.refrescar { (ok, error) in
            
            self.refreshControl.endRefreshing()
            
            if ok {
                self.rutas = Mapper<RutaModel>().mapArray(JSONObject: Defaults[.rutas] )!
                self.initTable()
                self.table.reloadData()
            }else{
                self.showAlert(error){
                    self.cerrarSesion()
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func iniciarServicio(_ ruta: RutaModel) {
        
        if ruta.actividades.count > 0 {
            Defaults[.ruta_activa] = ruta.toJSON()
            let controller = configuracionServicioViewController.instance(fromAppStoryboard: .configuracionServicio)
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            showAlert("¿Seguro que deseas iniciar el servicio \(ruta_activa.nombre.uppercased()) ?",textAceptar:"SÍ",textCancelar:"NO", callback: {
                ZVProgressHUD.setMaskType(.black)
                ZVProgressHUD.show()
                
                
                FirebaseDB.nuevoServicio(self.ruta_activa) { (ok, response) in
                    ZVProgressHUD.dismiss()
                    if ok {
                        let controller = ServicioViewController.instance(fromAppStoryboard: .Servicio)
                        self.navigationController?.pushViewController(controller, animated: true)
                    }else{
                        self.showAlert(response)
                    }
                }
            }, callbackCancel: nil);
        }
        
        
    }
    
    
    func compartirServicio(_ ruta: RutaModel) {
        compartir(ruta: ruta)
    }
    
    func configurarServicio(_ ruta: RutaModel){
        let controller = UINavigationController.instance(fromAppStoryboard: .configuracionServicio)
        Routing.show(controller)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RutasViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! CellGenerica
        
        if (item.cell == CellType.ruta){
            let ruta = RutaModel(JSON: item.data)!
            
            cell.label1.text = ruta.nombre
            cell.label2.text = ruta.vehiculo
            cell.label3.text = ruta.empresa
            cell.label4.text = ruta.descripcion
            
            cell.btnPM1.addAction {
                self.iniciarServicio(ruta)
            }
            cell.btnPM2.addAction {
                self.compartirServicio(ruta)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = container[indexPath.row]
        
        if (item.cell == CellType.ruta){
            return 260
        }else if (item.cell == CellType.blanco){
            return 30
        }else{
            return 0
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let item = rutas[indexPath.row]
        
        
    }
    
}
