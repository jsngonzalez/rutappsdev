//
//  HomeViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 18/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class HomeViewController: UIViewController {

    @IBOutlet var lblNombre: UILabel!
    @IBOutlet var btnSalir: UIButton!
    @IBOutlet var lblVersion: UILabel!
    
    @IBOutlet var btnRutas: UIButton!
    @IBOutlet var btnInspeccion: UIButton!
    @IBOutlet var btnDocumentos: UIButton!
    @IBOutlet var btnDocumentosVehiculos: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView(){
    
        lblNombre.text = usuario.nombre
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        lblVersion.text = appVersion
        
        btnSalir.addAction {
            self.salir()
        }
        
        
        btnRutas.addAction {
            let controller = RutasViewController.instance(fromAppStoryboard: .Rutas)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        
        btnInspeccion.addAction {
            self.mostrarPlacas(){
                self.inspeccionVehiculo()
            }
        }
        
        
        btnDocumentos.addAction {
            Defaults.remove(.placa_activa)
            let controller = DocumentosViewController.instance(fromAppStoryboard: .Documentos)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        btnDocumentosVehiculos.addAction {
            self.mostrarPlacas(){
                let controller = DocumentosViewController.instance(fromAppStoryboard: .Documentos)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        
    }
    
    func mostrarPlacas(callback: @escaping (()->Void) ){
        
        if placas.count == 0 {
            showAlert("No tiene vehículos asignados.")
        }else if placas.count == 1 {
            Defaults[.placa_activa] = placas[0].toJSON()
            callback()
        }else{
            
            var titles = placas.map({ (item) -> String in
                return item.placa
            })
            
            var idsVehiculos = placas.map({ (item) -> String in
                return item.idVehiculo
            })
            
            select(titles,titulo: "Seleccione una placa") { (selectedIndexes, selectedValues) in
                let idSeleccionado = idsVehiculos[selectedIndexes.first!]
                let placaSeleccionada = titles[selectedIndexes.first!]
                
                let placa = PlacaModel(JSON: ["idVehiculo":idSeleccionado,"placa":placaSeleccionada])!
                Defaults[.placa_activa] = placa.toJSON()
                
                callback()
                
            }
        }
    }
    
    func inspeccionVehiculo(){
        let controller = InspeccionesViewController.instance(fromAppStoryboard: .Inspecciones)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func salir(){
        
        showAlert("¿Deseas cerrar sesión?", callback: {
            self.cerrarSesion()
        }, callbackCancel:nil)
        
    }
    
    
    func cerrarSesion(){
        Defaults.removeAll()
        let controller = LoginViewController.instance(fromAppStoryboard: .Login)
        Routing.show(controller)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
