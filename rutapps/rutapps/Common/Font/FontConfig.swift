//
//  Fonts.swift
//  rutapps
//
//  Created by IMAC on 1/08/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit

enum fontConfig: String {
    case OpenSans
    
    var Bold: String {
        switch self {
        case .OpenSans: return "OpenSans-Bold"
        }
    }
    
    var light: String {
        switch self {
        case .OpenSans: return "OpenSans-Light"
        }
    }
    
    var Regular: String {
        switch self {
        case .OpenSans: return "OpenSans-Regular"
        }
    }
}
