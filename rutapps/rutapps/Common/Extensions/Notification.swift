//
//  Notification.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let none = Notification.Name("")
    static let mensaje = Notification.Name("mensaje")
    static let actualizar_servicio = Notification.Name("actualizar_servicio")
    static let pushNoti = Notification.Name("pushNoti")
}
