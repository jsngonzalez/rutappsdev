//
//  ArchivosManager.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 29/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults
import Crashlytics

class DocumentosManager: NSObject {
    
    class func refrescar(callback:@escaping (Bool,String) -> Void = {(ok,error) in} ){
        
        let vehiculo = PlacaModel(JSON: Defaults[.placa_activa] )!
        let url = API.documentos +  "?idVehiculo=" + String(vehiculo.idVehiculo)

        Servicios().get(url, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    class func guardar(_ archivo:ArchivoModel, callback:@escaping (Bool,String) -> Void = {(ok,error) in} ){
        
        
        let vehiculo = PlacaModel(JSON: Defaults[.placa_activa] )!
        
        let frmArchivo = FrmArchivoModel(JSON: [:])!
        frmArchivo.idTipo = archivo.idTipo
        frmArchivo.archivos = archivo.archivos
        frmArchivo.idVehiculo = vehiculo.idVehiculo
        frmArchivo.dia = archivo.dia
        frmArchivo.mes = archivo.mes
        frmArchivo.anio = archivo.anio
        frmArchivo.mensaje = archivo.mensaje
        
        let req = RequestModel(JSON: ["data":frmArchivo.toJSON()] )!
        
        Servicios().post(API.documentos, showLoading:true, parameters: req, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    
    
    class func validar(_ json:[String:Any]) -> Bool{
        
        let response = MapResponse.toArray(json, with: DocumentosModel.self)
        if response.error == 0, let response = response.response {
            Defaults[.documentos] = response.toJSON()
            return true
        }else{
            Defaults.remove(.documentos)
            return false
        }
        
    }
}
