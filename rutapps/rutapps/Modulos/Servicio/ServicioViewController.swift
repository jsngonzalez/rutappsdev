//
//  ServicioViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 25/12/17.
//  Copyright © 2017 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import SwiftLocation
import PMSuperButton
import Firebase
import AlamofireImage
import CoreLocation
import SwiftyUserDefaults
import ObjectMapper
import Notifwift
import Lottie
import ZVProgressHUD
import SimpleAnimation

class ServicioViewController: MasterUIViewController {
    
    
    @IBOutlet var table: UITableView!
    @IBOutlet var lblNombreRuta: UILabel!
    @IBOutlet var lblCantidadActividades: UILabel!
    
    @IBOutlet var viewContentOpciones: SuperView!
    @IBOutlet var viewContentMasOpciones: UIView!
    @IBOutlet var btnCompartir: PMSuperButton!
    @IBOutlet var btnNuevo: PMSuperButton!
    @IBOutlet var btnTerminarServicio: PMSuperButton!
    
    @IBOutlet var btnVerMapa: PMSuperButton!
    @IBOutlet var btnMensajes: PMSuperButton!
    @IBOutlet var btnAbrirCerrarView: PMSuperButton!
    
    
    struct CellType {
        static let blanco = "CellBlank"
        static let actividad = "CellActividad"
        
    }
    
    let notifwift = Notifwift()
    var actividades = [ActividadModel]()
    
    var container:[(cell:String,data:[String:Any])]=[]
    
    var listenerServicio : ListenerRegistration?
    var listenerActividades : ListenerRegistration?
    var listenerEstados : ListenerRegistration?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        initView()
        initFireStore()
        //initGPS()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initGPS()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewContentOpciones.bounceIn(from: .bottom)
    }
    
    func initFireStore(){
        
        listenerServicio = servicioRef.addSnapshotListener { (documentSnapshot, error) in
            
            if let document = documentSnapshot, let data = document.data() {
                let ruta = RutaModel(JSON: data)!
                
                if ruta.estado == FirebaseServiceType.terminado {
                    self.irAlHome()
                }else{
                    Defaults[.ruta_activa] = ruta.toJSON()
                    self.initInfoRuta()
                }
                
            }else{
                self.irAlHome()
            }
        }
        
       
        listenerActividades = actividadesRef.order(by: "orden", descending: false).addSnapshotListener { (querySnapshot, error) in
            
            if let error = error {
                print("Error retreiving collection: \(error)")
            }else{
                
                self.actividades = [ActividadModel]()
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    let nuevo = ActividadModel(JSON: document.data())!
                    self.actividades.append(nuevo)
                }
                
                self.initActividades()
            }
        }
        
        
        listenerEstados = estadosRef.addSnapshotListener { (querySnapshot, error) in
            
            if let error = error {
                print("Error retreiving collection: \(error)")
            }else{
                
                var estados = [EstadoModel]()
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    let nuevo = EstadoModel(JSON: document.data())!
                    estados.append(nuevo)
                }
                
                Defaults[.estados] = estados.toJSON()
                
            }
        }
        
    }
    
    func initInfoRuta(){
        lblNombreRuta.text = ruta_activa.nombre
    }
    
    func initActividades(){
        
        container = [(cell:String,data:[String:Any])]()
        container.append((cell: CellType.blanco, data: [:] ))
        
        let txtActividad = (actividades.count == 1) ? "actividad" : "actividades"
        
        lblCantidadActividades.text = "Tienes \(actividades.count) \(txtActividad) para hoy"
        for item in actividades {
            container.append((cell: CellType.actividad, data: item.toJSON() ))
        }
        
        table.reloadData()
    }

    
    func initView(){
        
        sincronizarAlertas()
        initRemoteConfig()
        fetchRemoteConfig()
        
        btnTerminarServicio.touchUpInside{
            self.terminarServicio()
        }
        
        btnNuevo.touchUpInside{
            Routing.nuevaAlerta()
        }
        
        
        btnCompartir.touchUpInside{
            //link servicio
            self.compartir(ruta: self.ruta_activa)
        }
        
        btnAbrirCerrarView.touchUpInside {
            self.abrirCerarView()
        }
        
        
        btnVerMapa.touchUpInside{
            let controller = MapaViewController.instance(fromAppStoryboard: .Mapa)
            controller.url = self.ruta_activa.urlMapa
            Routing.popup(controller)
        }
        
        btnMensajes.touchUpInside{
            let controller = MensajesViewController.instance(fromAppStoryboard: .Mensajes)
            Routing.popup(controller)
        }
        
        initInfoRuta()
        
    }
    
    func abrirCerarView(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            var height = CGFloat(0)
            
            if self.viewContentOpciones.tag == 0 {
                height = 35.0
                self.viewContentMasOpciones.layer.opacity = 0
                self.viewContentOpciones.tag = 1
                self.btnAbrirCerrarView.setTitle("Abrir más opciones", for: .normal)
            }else{
                height = 152.0
                self.viewContentMasOpciones.layer.opacity = 1
                self.viewContentOpciones.tag = 0
                self.btnAbrirCerrarView.setTitle("Cerrar opciones", for: .normal)
            }
            
            self.viewContentOpciones.frame.size.height = height
            self.viewContentOpciones.frame.origin.y = self.screen_height - (height + 20) //pading de 20
            
            
        })
    }
    
    
    func initGPS(){
        
        if appDelagete.solicitarGPS() {
            showAlert(.txt_error_gps, textAceptar: "Habilitar", textCancelar: "Cancelar", callback: {
                self.initLocation()
                
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                
            }, callbackCancel: nil)
        }else{
            initLocation()
        }
        
        
    }
    
    func initLocation(){
        
        LocationManager.shared.requireUserAuthorization(.always)
        
        appDelagete.locationManager = LocationManager.shared.locateFromGPS(.continous, accuracy: .neighborhood) { result in
            switch result {
            case .failure(let error):
                debugPrint("Received error: \(error)")
            case .success(let location):
                debugPrint("Location received: \(location)")
                
                if !Defaults.hasKey(.lastLocation) {
                    FirebaseService.agregarPunto(location)
                }
                
                FirebaseService.nuevoPunto(location)
                
            }
        }
    }
    
    func terminarServicio(){
        
        showAlert("¿Seguro que deseas terminar el servicio?",textAceptar:"SÍ",textCancelar:"CANCELAR", callback: {
            self.irAlHome()
        }, callbackCancel: nil);
        
    }
    
    func irAlHome(){
        
        servicioRef.updateData(["estado":FirebaseServiceType.terminado])
        
        NotificationCenter.default.removeObserver(self)
        
        listenerEstados?.remove()
        listenerServicio?.remove()
        listenerActividades?.remove()
        
        appDelagete.locationManager?.stop()
        appDelagete.backgroundLlocationManager?.stop()
        
        Defaults.remove(.ruta_activa)
        Defaults.remove(.lastLocation)
        
        
        let home = UINavigationController.instance(fromAppStoryboard: .Home)
        let rutas = RutasViewController.instance(fromAppStoryboard: .Rutas)
        home.pushViewController(rutas, animated: false)
        Routing.show(home)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cambiarEstado(actividad:ActividadModel) {
        
        Defaults[.actividad_activa] = actividad.toJSON()
        mostrarEstados()
        
    }
    
    
    func enviarEstado(_ estado:EstadoModel, actividad:ActividadModel){
        
        Defaults[.actividad_activa] = actividad.toJSON()
        
        estado.mensaje = ""
        var data = estado.toJSON()
        data = FirebaseService.agregarMetadatos(data)
        
        ZVProgressHUD.setMaskType(.black)
        ZVProgressHUD.show()
        
        estadosActividadRef.addDocument(data: data) { (error) in
            ZVProgressHUD.dismiss()
            self.actividadRef.updateData(["estado":estado.nombre])
            if error != nil {
                self.showAlert("Hubo un error al intentar enviar la información")
            }else{
                self.alertStatus("Información enviada exitosamente")
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ServicioViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item=container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! CellGenerica
        
        if (item.cell == CellType.actividad){
            let actividad = ActividadModel(JSON: item.data)!
            
            cell.label1.text = actividad.nombre
            cell.label2.text = actividad.descripcion
            cell.label3.text = actividad.estado
            
            cell.btn1.removeActions(for: .allEvents)
            cell.btnPM1.removeActions(for: .allEvents)
            
            cell.btn1.addAction {
                self.cambiarEstado(actividad: actividad)
            }
            
            cell.btnPM1.addAction {
                self.compartir(actividad: actividad)
            }
            
            if actividad.atajos.count > 0 {
                cell.pintarAtajos(actividad.atajos, actividad: actividad, servicioViewController: self)
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = container[indexPath.row]
        
        if (item.cell == CellType.actividad){
            
            let actividad = ActividadModel(JSON: item.data)!
            if actividad.atajos.count > 0 {
                return 240
            }else{
                return 178
            }
            
        }else if (item.cell == CellType.blanco){
            return 30
        }else{
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: screen_width, height: 150))
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    
    
}


extension ServicioViewController: CLLocationManagerDelegate {
    
    
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        
        
    }
    
}
