//
//  LoginViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 22/12/17.
//  Copyright © 2017 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import PMSuperButton
import SwiftyUserDefaults
import Firebase
import FirebaseAuth
import SwiftLocation

class LoginViewController: MasterUIViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnLogin: PMSuperButton!
    @IBOutlet var txtDocumento: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtDocumento.text=""
        txtDocumento.becomeFirstResponder()
    }
    
    
    func initView(){
        
        LocationManager.shared.requireUserAuthorization(.always)
        
        contentView.cornerAndShadow(UIColor.gray,bgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        
        btnLogin.touchUpInside {
            print("This button was pressed!")
            self.validar()
        }
        
        if Defaults.hasKey(.usuario) {
            
            if  Defaults.hasKey(.version) {
                let version =  Defaults[.version]
                let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                
                if version == currentVersion {
                    let controller = UINavigationController.instance(fromAppStoryboard: .Home)
                    Routing.show(controller)
                }else{
                    login()
                }
                
            }else{
                login()
            }
        }
        
    }
    
    func validar() {
        var error = false
        var mensaje = ""
        view.endEditing(true)
        
        if txtDocumento.text == "" {
            error=true
            mensaje="Debes ingresar tu documento de identidad."
        }
        
        if error {
            self.showAlert(mensaje)
        }else{
            login()
        }
        
    }
    
    func login() {
        
        UserManager.login(usuario: txtDocumento.text!, clave: txtDocumento.text!) { (ok, error) in
            if ok {
                
                let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                Defaults[.version] = currentVersion ?? "xyz"
                let controller = UINavigationController.instance(fromAppStoryboard: .Home)
                Routing.show(controller)
            }else{
                self.showAlert(error)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
