//
//  ModalViewController.swift
//  rutapps
//
//  Created by IMAC on 12/07/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import PMSuperButton

class ModalViewController: UIViewController {
    
    @IBOutlet var viewContent: UIView!
    @IBOutlet var btnAceptar: PMSuperButton!
    @IBOutlet var btnCancelar: PMSuperButton!
    @IBOutlet var lblMensaje: UILabel!
    
    
    @IBOutlet var lblTitulo: UILabel!
    @IBOutlet var txtDescripcion: UITextView!
    
    var titulo = ""
    var textAceptar = "Aceptar"
    var textCancelar = "Cancelar"
    var type: ModalActionType!
    var callbackOK: ModalActionHandler?
    var callbackOKTexView: ModalActionTextViewHandler?
    var callbackError: ModalActionHandler?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initView()
        agregarBlur()
        
        if let typeAction = type {
            switch typeAction {
                case .alert:
                    
                    initAlert()
                
                case .confirm:
                    
                    initConfirm()
                
                case .textview:
                    
                    initTextView()
            }
        }else{
            cerrarModal()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if type == .textview {
            txtDescripcion.text = ""
            txtDescripcion.becomeFirstResponder()
        }
    }
    
    
    func agregarBlur(){
        
        self.viewContent.sombra()
        self.view.insertSubview(blureffect(), at: 0)
    }
    
    
    func initAlert(){
        btnAceptar.touchUpInside {
            self.cerrarOKModal()
        }
    }
    
    func initConfirm(){
    
        btnAceptar.touchUpInside {
            self.cerrarOKModal()
        }
        
        btnCancelar.touchUpInside {
            self.cerrarModal()
        }
        
    }
    
    func initTextView(){
        
        
        btnAceptar.touchUpInside {
            self.cerrarOKModal()
        }
        
        btnCancelar.touchUpInside {
            self.cerrarModal()
        }
        
        activarNotificaciones()
    }
    
    func activarNotificaciones(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func initSelect(){
        btnCancelar.touchUpInside {
            self.cerrarModal()
        }
    }
    
    func initView(){
        
        
        if let mensaje = lblMensaje{
            mensaje.text = titulo
        }
        
        
        if let lbl = lblTitulo{
            lbl.text = titulo
        }
        
        if let btn1 = btnAceptar {
            self.view.endEditing(true)
            btn1.setTitle(textAceptar, for: .normal)
        }
        
        if let btn2 = btnCancelar {
            self.view.endEditing(true)
            btn2.setTitle(textCancelar, for: .normal)
        }
        
        
        viewContent.frame = CGRect(x: viewContent.frame.origin.x, y: screen_height, width: viewContent.frame.size.width , height: viewContent.frame.size.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let heigth = viewContent.frame.size.height + 20
        let destination = screen_height - heigth
        
        UIView.animate(withDuration: 0.2) {
            var frame = self.viewContent.frame
            frame.origin.y = destination
            self.viewContent.frame = frame
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.2) {
            var frame = self.viewContent.frame
            frame.origin.y = self.screen_height
            self.viewContent.frame = frame
        }
        
        super.viewWillDisappear(animated)
    }
    
    func cerrarModal(){
        self.dismiss(animated: true, completion: {
            if (self.callbackError != nil) {
                self.callbackError!()
            }
        })
    }
    
    func cerrarOKModal(){
        self.dismiss(animated: true, completion: {
            if (self.callbackOK != nil) {
                self.callbackOK!()
            }else if (self.callbackOKTexView != nil) {
                self.callbackOKTexView!(self.txtDescripcion.text)
            }
        })
    }
    
    
    @IBAction func cancelar(_ sender: Any) {
        cerrarModal()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            let textFieldHeight = screen_height - keyboardHeight - 240
            
            UIView.animate(withDuration: 0.6, delay: 0,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.0,
                           options: .curveLinear,
                           animations: {
                            self.viewContent.frame = CGRect(x: self.viewContent.frame.origin.x, y: textFieldHeight, width: self.viewContent.frame.size.width , height: self.viewContent.frame.size.height)
            })
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        UIView.animate(withDuration: 0.6, delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.0,
                       options: .curveLinear,
                       animations: {
                        
                        let heigth = self.viewContent.frame.size.height + 20
                        let destination = self.screen_height - heigth
                        
                        var frame = self.viewContent.frame
                        frame.origin.y = destination
                        self.viewContent.frame = frame
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
