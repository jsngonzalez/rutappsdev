//
//  EstadoModel.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 8/5/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class EstadoModel: Mappable {
    
    var idEstado = 0
    var nombre = ""
    var mensaje = ""
    var archivo = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map)
    {
        idEstado <- map["idEstado"]
        nombre <- map["nombre"]
        mensaje <- map["mensaje"]
        archivo <- map["archivo"]
    }
    
}
