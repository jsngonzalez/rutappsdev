//
//  PushModel.swift
//  rutappsusuario
//
//  Created by Jeisson Gonzalez on 2/19/19.
//  Copyright © 2019 devops. All rights reserved.
//

import UIKit
import ObjectMapper

class PushModel: Mappable {
    
    var tipo = ""
    var idRuta = "0"
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        tipo <- map["tipo"]
        idRuta <- map["idRuta"]
    }
}
