//
//  UIViewController+extension.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import ZVProgressHUD
import ObjectMapper
import Firebase
import Notifwift
import StatusAlert
import YPImagePicker

extension UIViewController {
    
    var appDelagete:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var usuario:UsuarioModel {
        
        var usuario = UsuarioModel(JSON: [:] )!
        if (Defaults.hasKey(.usuario)){
            usuario = UsuarioModel(JSON: (Defaults[.usuario]) )!
        }
        return usuario
    }
    
    var placas:[PlacaModel] {
        
        var list = [PlacaModel]()
        if (Defaults.hasKey(.placas)){
            list =  Mapper<PlacaModel>().mapArray(JSONObject: (Defaults[.placas]) )!
        }
        return list
    }
    
    
    var placa:PlacaModel {
        
        var ruta = PlacaModel(JSON: [:] )!
        if (Defaults.hasKey(.placa_activa)){
            ruta = PlacaModel(JSON: (Defaults[.placa_activa]) )!
        }
        return ruta
    }
    
    var ruta_activa:RutaModel {
        
        var ruta = RutaModel(JSON: [:] )!
        if (Defaults.hasKey(.ruta_activa)){
            ruta = RutaModel(JSON: (Defaults[.ruta_activa]) )!
        }
        return ruta
    }
    
    var actividad_activa:ActividadModel {
        
        var actividad = ActividadModel(JSON: [:] )!
        if (Defaults.hasKey(.actividad_activa)){
            actividad = ActividadModel(JSON: (Defaults[.actividad_activa]) )!
        }
        return actividad
    }
    
    var estados_actividad:[EstadoModel] {
        
        var list = [EstadoModel]()
        if (Defaults.hasKey(.estados)){
           list =  Mapper<EstadoModel>().mapArray(JSONObject: (Defaults[.estados]) )!
        }
        return list
    }
    
    var inspeccion_activa:DiasModel {
        
        var item = DiasModel(JSON:[:])!
        if (Defaults.hasKey(.inspeccion_activa)){
            item = DiasModel(JSON: (Defaults[.inspeccion_activa]) )!
        }
        return item
    }
    
    var estado_activo:EstadoModel {
        
        var estado = EstadoModel(JSON: [:] )!
        if (Defaults.hasKey(.estado_activo)){
            estado = EstadoModel(JSON: (Defaults[.estado_activo]) )!
        }
        return estado
    }
    
    var servicioRef:DocumentReference {
        return FirebaseDB.db().collection("rutas").document(ruta_activa.idServicio)
    }
    
    
    var alertasRef:CollectionReference {
        return FirebaseDB.db().collection("rutas").document(ruta_activa.idServicio).collection("alertas")
    }
    
    var actividadesRef:CollectionReference {
        return FirebaseDB.db().collection("rutas").document(ruta_activa.idServicio).collection("actividades")
    }
    
    var estadosRef:CollectionReference {
        return FirebaseDB.db().collection("rutas").document(ruta_activa.idServicio).collection("estados")
    }
    
    var estadosActividadRef:CollectionReference {
        return actividadesRef.document("actividad_\(actividad_activa.idActividad)").collection("estados")
    }
    
    var actividadRef:DocumentReference {
        return actividadesRef.document("actividad_\(actividad_activa.idActividad)")
    }
    
    var screen_width : CGFloat {
        return UIScreen.main.bounds.width
    }
    
    var screen_height : CGFloat{
        return UIScreen.main.bounds.height
    }
    
    func analyticsRegisterScreen(nameSceen: String) {
        
        
    }
    
    func pushButtonEvent(eventName: String) {
        
    }
    
    func trackEvent(paramethers: (category: String?, action: String?)) {
        
        
    }
    
    func alertStatus(_ message:String){
        let statusAlert = StatusAlert()
        statusAlert.message = message
        statusAlert.canBePickedOrDismissed = true
        statusAlert.showInKeyWindow()
    }
    
    func showAlert(_ message: String, textAceptar:String = "OK") {
        Modal.showAlert(message, type: .alert, textAceptar: textAceptar)
    }
    
    func showAlert(_ message: String,textAceptar:String = "OK", callback: (() -> Void)? ) {
        
        Modal.showAlert(message, type: .alert, textAceptar: textAceptar, handlerOK: {
            if callback != nil {
                callback!()
            }
        })
        
    }
    
    
    func showTextView(_ titulo: String,textAceptar:String = "OK", callback: ((String) -> Void)? ) {
        
        Modal.textView(titulo, textAceptar: textAceptar, handlerOK: { (texto) in
            if callback != nil {
                callback!(texto)
            }
        })
        
    }
    
    func showLoading(){
        ZVProgressHUD.setMaskType(.black)
        ZVProgressHUD.show()
    }
    
    func hideLoading(){
        ZVProgressHUD.dismiss()
    }
    
    func select(_ list: [String],titulo:String = "Selecciona", callback: ((_ selectedIndexes:[Int], _ selectedValues:[String]) -> Void)? ) {
        
        let regularFont =  UIFont.init(name: fontConfig.OpenSans.Regular, size: 16)!
        let boldFont = UIFont.init(name: fontConfig.OpenSans.Bold, size: 16)!
        let btnColor = #colorLiteral(red: 1, green: 0.6639655232, blue: 0.1801092923, alpha: 1)
        
        let blueAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : titulo,
            titleFont           : boldFont,
            titleTextColor      : .black,
            titleBackground     : .clear,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Buscar",
            closeButtonTitle    : "Cancelar",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "OK",
            doneButtonColor     : btnColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"blue_ic_checked"),
            itemUncheckedImage  : UIImage(),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        
        
        let picker = YBTextPicker.init(with: list, appearance: blueAppearance, onCompletion: { (selectedIndexes, selectedValues) in
            if selectedValues.count > 0{
                if let callback = callback {
                    callback(selectedIndexes,selectedValues)
                }
            }
        },onCancel: {
            print("Cancelled")
        })
        
        picker.allowMultipleSelection = false
        
        picker.show(withAnimation: .Fade)
        
    }
    
    func showAlert(_ message: String,textAceptar:String = "SÍ", textCancelar:String = "NO", callback: (() -> Void)?, callbackCancel:(() -> Void)? ) {
        
        Modal.showAlert(message, type: .confirm, textAceptar: textAceptar, textCancelar: textCancelar, handlerOK: {
            if callback != nil {
                callback!()
            }
        }, handlerError: {
            if callbackCancel != nil {
                callbackCancel!()
            }
        })
        
    }
    
    func shared(link: String){
        let activityViewController = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    func sincronizarAlertas(){
        AlertasManager.refrescar()
    }
    
    func blureffect() -> UIVisualEffectView{
        let blurEffect = UIBlurEffect(style: .regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return blurEffectView
    }
    
    func fetchRemoteConfig() {
        RemoteConfig.remoteConfig().fetch { (status, error) in
            guard error != nil else {
                print("error!!!")
                return
            }
            
            RemoteConfig.remoteConfig().activate { _, error in
                if error != nil {
                    print("error!!!")
                }else{
                    print("Data activate!!")
                }
            }
        }
        
    }
    
    func initRemoteConfig(){
        let values = [
            "LOCATION_MIN_DISTANCE_CHANGED":50 as NSObject
        ]
        
        RemoteConfig.remoteConfig().setDefaults(values)
    }
    
    func getHoraAmPm() -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let dateString = formatter.string(from: Date())
        return dateString
    }
    
    func compartir(actividad:ActividadModel) {
        
        print("Compartir actividad")
        print(actividad)
        
        let controller = CompartirServicioViewController.instance(fromAppStoryboard: .CompartirServicio)
        controller.codigo = actividad.codigo
        controller.link = actividad.link
        
        Routing.popup(controller)
        
    }
    
    func compartir(ruta:RutaModel) {
        
        print("Compartir ruta")
        print(ruta_activa)
        
        let controller = CompartirServicioViewController.instance(fromAppStoryboard: .CompartirServicio)
        controller.codigo = ruta.codigo
        controller.link = ruta.link
        
        Routing.popup(controller)
        
    }
    
    func mostrarEstados(){
        
        if estados_actividad.count > 0 {
            Routing.nuevoEstado()
        }else{
            showAlert("No existen estados adicionales para esta ruta")
        }
        
    }
    
    func subirFotoFirebase(path:String, callback:((String,UIImage) -> Void)! ){
        
        
        
        let coloredImage = UIImage(color: #colorLiteral(red: 1, green: 0.6, blue: 0.1411764706, alpha: 1))
        UINavigationBar.appearance().setBackgroundImage(coloredImage, for: UIBarMetrics.default)
        
        let attributes = [NSAttributedString.Key.font : UIFont.init(name: fontConfig.OpenSans.Bold, size: 16)! ]
        UINavigationBar.appearance().titleTextAttributes = attributes // Title fonts
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal) // Bar Button fonts
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) ] // Title color
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // Left. bar buttons
        
        let picker = YPImagePicker()
        
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage ?? "") // Transformed image, can be nil
                print(photo.exifMeta ?? "") // Print exif meta data of original image.
                
                let image = photo.image
                
                let storage = Storage.storage()
                let storageRef = storage.reference()
                
                let archivosRef = storageRef.child("\(path)/\(UUID().uuidString).jpg")
                let metadata = StorageMetadata()
                metadata.contentType = "image/jpeg"
                
                var imageSend = image.fixedOrientation()
                imageSend = imageSend.resizeImage(600, opaque: true)
                
                let data = imageSend.jpegData(compressionQuality: 0.6)
                
                
                
                ZVProgressHUD.setMaskType(.black)
                ZVProgressHUD.show()
                
                let _ = archivosRef.putData(data!, metadata: metadata) { (metadata, error) in
                    
                    guard let _ = metadata else {
                        // Uh-oh, an error occurred!
                        ZVProgressHUD.dismiss()
                        self.showAlert("Error al intentar subir la imagen")
                        return
                    }
                    
                    archivosRef.downloadURL { (url, error) in
                        
                        ZVProgressHUD.dismiss()
                        
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            self.showAlert("Error al intentar subir la imagen")
                            return
                        }
                        
                        callback(downloadURL.absoluteString, image)
                    }
                }
                
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
    
    func nombreMes(_ index:Int) -> String{
        let meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Obtubre","Nomviembre","Diciembre"]
        return meses[(index-1)]
    }
    
    func arrayToJSON(_ array:[String]) -> String{
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: array, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
               return JSONString
            }else{
                return "[]"
            }
        } catch {
            return "[]"
        }
    }
    
}
