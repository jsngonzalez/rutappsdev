//
//  VehiculoModel.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 7/18/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class VehiculoModel: Mappable {
    
    var idVehiculo = 0
    var placa = ""
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map)
    {
        idVehiculo <- map["idVehiculo"]
        placa <- map["placa"]
    }
    
}
