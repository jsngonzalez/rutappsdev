//
//  UserDefaults+extension.swift
//  rutapps
//
//  Created by IMAC on 1/08/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

extension DefaultsKeys {
    static let playerID = DefaultsKey<String>("obj_playerID", defaultValue: "")
    static let alertas = DefaultsKey<[[String: Any]]>("obj_alertas", defaultValue: [])
    static let empresa = DefaultsKey<[String: Any]>("obj_empresa", defaultValue: [:])
    static let usuario = DefaultsKey<[String: Any]>("obj_usuario", defaultValue: [:])
    static let placas = DefaultsKey<[[String: Any]]>("obj_placas", defaultValue: [])
    static let login = DefaultsKey<[String: Any]>("obj_login", defaultValue: [:])
    static let pushNoti = DefaultsKey<[String: Any]>("obj_pushNoti", defaultValue: [:])
    static let rutas = DefaultsKey<[[String: Any]]>("obj_rutas", defaultValue: [])
    static let ruta_activa = DefaultsKey<[String: Any]>("obj_ruta_activa", defaultValue: [:])
    static let placa_activa = DefaultsKey<[String: Any]>("obj_placa_activa", defaultValue: [:])
    static let actividad_activa = DefaultsKey<[String: Any]>("obj_actividad_activa", defaultValue: [:])
    static let estados = DefaultsKey<[[String: Any]]>("obj_estados", defaultValue: [])
    static let inspecciones = DefaultsKey<[String: Any]>("obj_inspecciones", defaultValue: [:])
    static let inspeccion_activa = DefaultsKey<[String: Any]>("obj_inspeccion_activa", defaultValue: [:])
    static let estado_activo = DefaultsKey<[String: Any]>("obj_estado_activo", defaultValue: [:])
    
    static let documentos = DefaultsKey<[[String: Any]]>("obj_documentos", defaultValue: [])
    static let documento_activo = DefaultsKey<[String: Any]>("obj_documento_activo", defaultValue: [:])
    
    
    static let version = DefaultsKey<String>("obj_version", defaultValue: "")
    
    
    
    
    static let lastLocation = DefaultsKey<[String: Any]>("obj_lastLocation", defaultValue: [:])
}
