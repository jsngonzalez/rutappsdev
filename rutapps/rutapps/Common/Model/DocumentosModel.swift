
//
//  Inspecciones.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 20/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class DocumentosModel: Mappable {
    
    var idTipo = 0
    var idEmpresa = 0
    var nombre = ""
    var descripcion = ""
    var solicitar_fecha = 0
    var tipoDocumento = 0
    var tieneDocumento = 0
    var vencido = 0
    var archivo = ArchivoModel(JSON:[:])!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idTipo <- map["idTipo"]
        idEmpresa <- map["idEmpresa"]
        nombre <- map["nombre"]
        descripcion <- map["descripcion"]
        solicitar_fecha <- map["solicitar_fecha"]
        tipoDocumento <- map["tipoDocumento"]
        tieneDocumento <- map["tieneDocumento"]
        vencido <- map["vencido"]
        archivo <- map["archivo"]
    }
    
}

class ArchivoModel: Mappable {
    
    var idDocumento = 0
    var idTipo = 0
    var archivos = ""
    var mensaje = ""
    var dia = 0
    var mes = 0
    var anio = 0
    var vencido = 0
    var mostrarFecha = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idDocumento <- map["idDocumento"]
        idTipo <- map["idTipo"]
        archivos <- map["archivos"]
        mensaje <- map["mensaje"]
        dia <- map["dia"]
        mes <- map["mes"]
        anio <- map["anio"]
        vencido <- map["vencido"]
        mostrarFecha <- map["mostrarFecha"]
    }
    
}


class FrmArchivoModel: Mappable {
    
    var idTipo = 0
    var archivos = ""
    var mensaje = ""
    var dia = 0
    var mes = 0
    var anio = 0
    
    var idVehiculo = "0"
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        idTipo <- map["idTipo"]
        archivos <- map["archivos"]
        mensaje <- map["mensaje"]
        dia <- map["dia"]
        mes <- map["mes"]
        anio <- map["anio"]
        
        idVehiculo <- map["idVehiculo"]
    }
    
}

class FotoModel: Mappable {
    
    var id = ""
    var url = ""
    var image : UIImage!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        url <- map["url"]
        image <- map["image"]
    }
    
}

