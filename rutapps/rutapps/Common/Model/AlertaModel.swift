//
//  AlertaModel.swift
//  rutapps
//
//  Created by IMAC on 9/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit

import ObjectMapper

class AlertaModel: Mappable {
    
    var idAlerta = 0
    var nombre = ""
    var mensaje = ""
    var archivo = ""
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map)
    {
        idAlerta <- map["idAlerta"]
        nombre <- map["nombre"]
        mensaje <- map["mensaje"]
        archivo <- map["archivo"]
    }
    
}

