//
//  UsuarioModel.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class EmpresaModel: Mappable {
    
    var idEmpresa = 0
    var nombre = ""
    var rutas = [RutaModel]()
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        idEmpresa <- map["idEmpresa"]
        nombre <- map["nombre"]
        rutas <- map["rutas"]
    }
}
