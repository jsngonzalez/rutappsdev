//
//  FirebaseService.swift
//  rutapps
//
//  Created by IMAC on 10/07/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit

import Foundation
import SwiftyUserDefaults
import CoreLocation
import Firebase

struct FirebaseServiceType {
    static let terminado = "TERMINADO"
}

class FirebaseService: NSObject {
    
    class func nuevoPunto(_ latlong: CLLocation? = nil) {
        
        
        if Defaults.hasKey(.ruta_activa) {
            
            let rc = RemoteConfig.remoteConfig()
            let minDistancia:Double = rc["LOCATION_MIN_DISTANCE_CHANGED"].numberValue.doubleValue
            
            
            let loaded = Defaults[.lastLocation]
            let dataLastLocation = loaded["lastLocation"] as! Data
            
            let lastLocation = NSKeyedUnarchiver.unarchiveObject(with: dataLastLocation ) as! CLLocation
            
            
            var location = NSKeyedUnarchiver.unarchiveObject(with: dataLastLocation ) as! CLLocation
            UIDevice.current.isBatteryMonitoringEnabled = true
            
            //print("latitude lastLocation: \(lastLocation.coordinate.latitude) longitude lastLocation: \(lastLocation.coordinate.longitude)")
            
            if latlong != nil {
                location = latlong!
            }
            
            //print("latitude Newlocation: \(location.coordinate.latitude) longitude Newlocation: \(location.coordinate.longitude)")
            
            let distanceInMeters = (lastLocation.coordinate.latitude != 0) ? location.distance(from: lastLocation) : 0

            if  distanceInMeters > minDistancia {
                self.agregarPunto(location)
            }else{
                print("esta muy cerca para marcar otro punto: \(distanceInMeters)")
            }
        }else{
            print("No existe servicio marcar punto")
        }
        
        
    }
    
    class func agregarPunto(_ location: CLLocation){
        
        let ruta = RutaModel(JSON: (Defaults[.ruta_activa]) )!
        let servicioRef = FirebaseDB.db().collection("rutas").document(ruta.idServicio)
        let rutaRef = FirebaseDB.db().collection("rutas").document(ruta.idServicio).collection("ruta")
        
        let velocidad = location.speed < 0 ? 0 : location.speed * 3.6
        
        var punto:[String:Any] = [:]
        punto["velocidad"] = velocidad
        punto["geoPoint"] =  GeoPoint.init(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        punto["fecha"] = FieldValue.serverTimestamp()
        
        rutaRef.addDocument(data: punto)
        servicioRef.updateData(["localizacion":punto])
        
        let locationData = NSKeyedArchiver.archivedData(withRootObject: location)
        Defaults[.lastLocation] = [
            "lastLocation":locationData
        ]
    }
    
    
    class func agregarMetadatos(_ data:[String:Any]) -> [String:Any] {
        
        var data = data
        data["fecha"] = FieldValue.serverTimestamp()
        
        if Defaults.hasKey(.lastLocation) {
            let loaded = Defaults[.lastLocation]
            let dataLastLocation = loaded["lastLocation"] as! Data
            let lastLocation = NSKeyedUnarchiver.unarchiveObject(with: dataLastLocation ) as! CLLocation
            data["geoPoint"] =  GeoPoint.init(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude)
        }else{
            data["geoPoint"] =  GeoPoint.init(latitude: 0, longitude: 0)
        }
        
        return data
    }
    
}
