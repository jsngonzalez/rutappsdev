//
//  String+extension.swift
//  rutapps
//
//  Created by IMAC on 14/09/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

extension String
{
    func CGFloatValue() -> CGFloat? {
        guard let doubleValue = Double(self) else {
            return nil
        }
        
        return CGFloat(doubleValue)
    }
    
    func speach(){
        DispatchQueue.main.async {
            let utterance = AVSpeechUtterance(string: self)
            utterance.voice = AVSpeechSynthesisVoice(language: "es-CO")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
        }
    }
    
    func calculateSize(fontSize: CGFloat, maxWidth : CGFloat, numberOfLines: Int) -> CGRect{
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: maxWidth, height: CGFloat.leastNonzeroMagnitude))
        label.numberOfLines = numberOfLines
        label.font = UIFont(name: fontConfig.OpenSans.Regular , size: fontSize)!
        label.text = self
        
        label.sizeToFit()
        return label.frame
    }
    
}
