//
//  FirebaseDB.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 7/18/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit

import Firebase
import FirebaseFirestore
import SwiftyUserDefaults
import ObjectMapper
import Alamofire

class FirebaseDB: NSObject {
    
    class func db() -> Firestore {
        
        let db = Firestore.firestore()
        let settings = db.settings
        settings.isPersistenceEnabled = true
        settings.cacheSizeBytes = FirestoreCacheSizeUnlimited
        db.settings = settings
        
        return db
    }
    
    class func call(funcion: String = "", data: [String:Any] = [:], callback:@escaping (Bool, [String:Any]) -> Void )  {
        
        let url="https://us-central1-rutapps-183a3.cloudfunctions.net/\(funcion)";
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let data = [
            "data":data
            ] as [String:Any]
        
        print(url)
        print(data.toJson())
        
        Alamofire.request(url, method: .post, parameters: data, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.result.isSuccess {
                let JSON = response.result.value as! [String:Any]
                print(JSON)
                let error = (JSON["error"] as! Int)
                if error == 0 {
                    callback(true,["error":0, "response":JSON["response"] as Any ])
                }else{
                    callback(false,["error":1, "response":(JSON["response"] as! String)])
                }
            }else{
                callback(false,["error":1, "response":response.result.error?.localizedDescription ?? "Error desconocido."])
            }
            
        }
    }
    
    class func nuevoServicio(_ ruta:RutaModel, callback:@escaping (Bool,String) -> Void )  {
        
        let data = [
            "token":ruta.token
        ]
        
        FirebaseDB.call(funcion: "nuevoServicio", data: data ) { (ok, response) in
            if ok {
                let result = RutaModel(JSON: (response["response"] as! [String:Any])  )!
                ruta.activo = result.activo
                ruta.idServicio = result.idServicio
                Defaults[.ruta_activa] = ruta.toJSON()
                
                callback(true, "" )
            }else{
                callback(false, (response["response"] as! String) )
                
            }
        }
        
    }
    
    class func nuevoServicio(_ ruta:RutaModel,actividades:[ActividadModel], callback:@escaping (Bool,String) -> Void )  {
        
        let data = [
            "token":ruta.token,
            "actividades":actividades.toJSON()
        ] as [String:Any]
        
        FirebaseDB.call(funcion: "nuevoServicio", data: data ) { (ok, response) in
            if ok {
                let result = RutaModel(JSON: (response["response"] as! [String:Any])  )!
                ruta.activo = result.activo
                ruta.idServicio = result.idServicio
                Defaults[.ruta_activa] = ruta.toJSON()
                
                callback(true, "" )
            }else{
                callback(false, (response["response"] as! String) )
                
            }
        }
        
    }
    
}


