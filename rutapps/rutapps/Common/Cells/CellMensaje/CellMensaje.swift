//
//  CellMensaje.swift
//  rutappsusuario
//
//  Created by MacBook on 10/29/19.
//  Copyright © 2019 devops. All rights reserved.
//

import UIKit

class CellMensaje: UITableViewCell {

    @IBOutlet var nombre: UILabel!
    @IBOutlet var mensaje: UILabel!
    @IBOutlet var fecha: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
