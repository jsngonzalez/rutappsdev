//
//  UsuarioModel.swift
//  rutapps
//
//  Created by IMAC on 6/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class UsuarioModel: Mappable {
    var idEmpresa = ""
    var usuario = ""
    var clave = ""
    var nombre = ""
    var documento = ""
    var telefono = ""
    var token = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        idEmpresa <- map["idEmpresa"]
        usuario <- map["usuario"]
        clave <- map["clave"]
        nombre <- map["nombre"]
        documento <- map["documento"]
        telefono <- map["telefono"]
        token <- map["token"]
    }
}
