//
//  Inspecciones.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 20/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class InspeccionesModel: Mappable {
    
    var dias = [DiasModel]()
    var formulario = [FormularioModel]()
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        dias <- map["dias"]
        formulario <- map["formulario"]
    }
    
}

class DiasModel: Mappable {
    
    var nombre = ""
    var mostrar_formulario = 0
    var formulario = [FormularioModel]()
    var dia = 0
    var mes = 0
    var anio = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nombre <- map["nombre"]
        mostrar_formulario <- map["mostrar_formulario"]
        formulario <- map["formulario"]
        dia <- map["dia"]
        mes <- map["mes"]
        anio <- map["anio"]
    }
    
}



class FormularioModel: Mappable {
    
    var nombre = ""
    var inspecciones = [InspeccionModel]()
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nombre <- map["nombre"]
        inspecciones <- map["inspecciones"]
    }
    
}


class InspeccionModel: Mappable {
    
    var nombre = ""
    var idInspeccion = 0
    var respuesta = ""
    var observaciones = ""
    var opciones = [OpcionModel]()
    var validar = [String]()
    var idVehiculo = 0
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nombre <- map["nombre"]
        idInspeccion <- map["idInspeccion"]
        respuesta <- map["respuesta"]
        observaciones <- map["observaciones"]
        opciones <- map["opciones"]
        validar <- map["validar"]
        idVehiculo <- map["idVehiculo"]
    }
    
}

class OpcionModel: Mappable {
    
    var nombre = ""
    var id = ""
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nombre <- map["nombre"]
        id <- map["id"]
    }
    
}
