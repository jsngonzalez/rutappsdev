//
//  ResponseString.swift
//  rutapps
//
//  Created by IMAC on 6/04/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper

class ResponseObjectModel<T:Mappable>: Mappable {
    
    var error = 1
    var response : T?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        response <- map["response"]
    }
}


class ResponseArrayModel<T:Mappable>: Mappable {
    
    var error = 0
    var response : [T]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        response <- map["response"]
    }
}
