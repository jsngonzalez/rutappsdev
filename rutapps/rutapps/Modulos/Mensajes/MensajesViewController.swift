//
//  MensajesViewController.swift
//  rutappsusuario
//
//  Created by MacBook on 10/29/19.
//  Copyright © 2019 devops. All rights reserved.
//

import UIKit
import ObjectMapper


class MensajesViewController: UIViewController {
    
    @IBOutlet var table: UITableView!
    @IBOutlet var viewContainer: SuperView!
    
    var container : [MensajeModel] = []
    
    struct identificadorCell {
        static let mensajes = "CellMensaje"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewContainer.bounceIn(from: .bottom)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.viewContainer.frame.origin.y = self.screen_height
        }
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    func initView(){
        
        self.view.insertSubview(blureffect(), at: 0)
        
        //viewContainer.frame.origin.y = screen_height
        viewContainer.add(gesture: .multiSwipe(direction: .down, fingers: 1)) { (view: UIView) in
            self.cerrar()
        }
        
        
        let cellMensaje = UINib(nibName: identificadorCell.mensajes, bundle: nil)
        table.register(cellMensaje, forCellReuseIdentifier: identificadorCell.mensajes)
        
        loadData()
    }
    
    func loadData(){
        Servicios().get(API.mensajes, showLoading: true, callback:{ response in
            let res = MensajeModel(JSON:response)!
            if  res.error == 0 {
                self.container = Mapper<MensajeModel>().mapArray(JSONObject: (res.response as! [[String:Any]]) )!
                self.table.reloadData()
                
            }else{
                self.showAlert((res.response as! String))
            }
        })
    }
    
    @IBAction func cerrarmenu(_ sender: Any) {
        cerrar()
    }
    
    
    @IBAction func volver(_ sender: Any) {
        cerrar()
    }
    
    func cerrar(){
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MensajesViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item=container[indexPath.row]
        

        let cell = tableView.dequeueReusableCell(withIdentifier: identificadorCell.mensajes, for: indexPath) as! CellMensaje
        
        cell.fecha.text = item.reg
        cell.mensaje.text = item.mensaje
        
        if item.idActividad > 0 {
            cell.nombre.text = item.actividad.nombre
        }else{
            cell.nombre.text = item.ruta.nombre
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: table.frame.size.width, height: table.frame.size.height))
        view.backgroundColor = .clear
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 117
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = container[indexPath.row]
        print(item)
        
    }
}
