//
//  CargandoServicioActivoViewController.swift
//  rutapps
//
//  Created by MacBook on 5/23/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import Lottie
import SwiftyUserDefaults
import Firebase
import FirebaseAuth

class CargandoServicioActivoViewController: UIViewController {

    @IBOutlet weak var cargando: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cargando.loadLocalLottie("location");
        cargando.backgroundColor = #colorLiteral(red: 0.9811730981, green: 0.9876660705, blue: 0.9938254952, alpha: 1)
        
        
        if Defaults.hasKey(.ruta_activa) {
            initView()
        }else if Defaults.hasKey(.usuario) {
            login()
        }
    }
    
    func initView(){
        
        let ruta = RutaModel(JSON: Defaults[.ruta_activa])!
        
        print(ruta.toJSONString(prettyPrint: true) ?? "no hay ruta")
        
        if ruta.idServicio != "" {
            
            servicioRef.getDocument { (documentSnapshot, error) in
                if let document = documentSnapshot, let data = document.data() {
                    let ruta = RutaModel(JSON: data)!
                    if ruta.estado == FirebaseServiceType.terminado {
                        self.login()
                    }else{
                        let controller = UINavigationController.instance(fromAppStoryboard: .Servicio)
                        Routing.show(controller)
                    }
                }else{
                    self.login()
                }
            }
        }else{
            self.login()
        }
    }
    
    func login(){
        
        Defaults.remove(.ruta_activa)
    
        let controller = LoginViewController.instance(fromAppStoryboard: .Login)
        Routing.show(controller)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
