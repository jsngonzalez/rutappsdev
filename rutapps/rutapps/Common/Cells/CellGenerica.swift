//
//  CellGenerica.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 24/12/17.
//  Copyright © 2017 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import PMSuperButton

class CellGenerica: UITableViewCell {

    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var label4: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet var label5: UILabel!
    @IBOutlet var label6: UILabel!
    
    @IBOutlet var txt1: UITextField!
    
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    
    @IBOutlet weak var btnPM1: PMSuperButton!
    @IBOutlet weak var btnPM2: PMSuperButton!
    @IBOutlet weak var btnPM3: PMSuperButton!
    
    @IBOutlet weak var viewContentAcronimo: UIView!
    @IBOutlet weak var lblAcronimo: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .clear
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func pintarAtajos(_ atajos:[EstadoModel],actividad:ActividadModel, servicioViewController: ServicioViewController){
        print(atajos.toJSON())
        
        for subView in view1.subviews {
            subView.removeFromSuperview()
        }
        
        let frameScrollView = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        let scrollView = UIScrollView.init(frame: frameScrollView)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        
        
        let pading = CGFloat(15)
        var width = CGFloat(10)
        
        for item in atajos {
            
            let font = UIFont.init(name: fontConfig.OpenSans.Regular, size: 14)!
            let widthText = CGFloat(UILabel.textWidth(font: font, text: item.nombre)) + 20//pading de 10
            
            let frame = CGRect(x: width + pading, y: 10, width: widthText, height: 30)
            let nuevo = UIButton.init(frame: frame)
            
            nuevo.titleLabel?.font = font
            nuevo.setTitleColor(#colorLiteral(red: 0.1614608765, green: 0.1948977113, blue: 0.2560786903, alpha: 1), for: .normal)
            nuevo.setTitle(item.nombre, for: .normal)
            nuevo.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nuevo.layer.borderColor = #colorLiteral(red: 0.1614608765, green: 0.1948977113, blue: 0.2560786903, alpha: 1)// #colorLiteral(red: 1, green: 0.6, blue: 0.1411764706, alpha: 1)
            nuevo.layer.borderWidth = 1
            nuevo.layer.cornerRadius = 10
            
            nuevo.addTap { (tap) in
                servicioViewController.enviarEstado(item, actividad: actividad)
            }
            
            scrollView.addSubview(nuevo)
            
            width = width + pading + widthText
            scrollView.contentSize = CGSize(width: width, height: 50)
        }
        
        self.view1.addSubview(scrollView)
        
    }
    
    func pintarOpcionesFormulario(_ opcion:InspeccionModel, viewController: InspeccionViewController){
        print(opcion.toJSON())
        
        label2.text = opcion.respuesta
        for subView in view1.subviews {
            if subView is UIButton {
                subView.removeFromSuperview()
            }
        }
        
        let frameScrollView = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 30, height: 77)
        let scrollView = UIScrollView.init(frame: frameScrollView)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        
        
        let pading = CGFloat(15)
        let width = CGFloat(50)
        
        for (index,item) in opcion.opciones.enumerated() {
            
            let font = UIFont.init(name: fontConfig.OpenSans.Bold, size: 14)!
            
            let frame = CGRect(x: (width * CGFloat(index) ) + (pading * CGFloat(index)), y: 27, width: width, height: width)
            let nuevo = UIButton.init(frame: frame)
            
            nuevo.titleLabel?.font = font
            nuevo.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            nuevo.setTitle(item.id, for: .normal)
            
            if item.nombre == opcion.respuesta {
                nuevo.backgroundColor = #colorLiteral(red: 0.5529411765, green: 0.7764705882, blue: 0.2470588235, alpha: 1)
            }else{
                nuevo.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
            
            nuevo.layer.cornerRadius = 25
            
            nuevo.addTap { (tap) in
                
                var mostrarMotivo = false
                for validar in opcion.validar {
                    if validar == item.id {
                        mostrarMotivo = true
                    }
                }
                
                if mostrarMotivo {
                    viewController.showTextView("Debe ingresar un motivo", textAceptar: "Guardar", callback: { (texto) in
                        
                        if texto != "" {
                            opcion.respuesta = item.nombre
                            opcion.observaciones = texto
                            viewController.guardarRespuesta(opcion)
                            self.pintarOpcionesFormulario(opcion, viewController: viewController)
                        }else{
                            viewController.showAlert("Debe ingresar un motivo para el estado \(item.nombre)")
                        }
                        
                    })
                    
                }else{
                    opcion.respuesta = item.nombre
                    opcion.observaciones = ""
                    viewController.guardarRespuesta(opcion)
                    self.pintarOpcionesFormulario(opcion, viewController: viewController)
                }
            }
            
            scrollView.addSubview(nuevo)
            scrollView.contentSize = CGSize(width: width, height: 50)
        }
        
        self.view1.addSubview(scrollView)
        
    }

}
