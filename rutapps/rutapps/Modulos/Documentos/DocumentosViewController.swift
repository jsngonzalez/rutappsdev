//
//  DocumentosViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 18/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ZVProgressHUD
import Firebase
import SwiftyUserDefaults
import ObjectMapper
import Actions

class DocumentosViewController: MasterUIViewController {

    @IBOutlet var table: UITableView!
    @IBOutlet var btnSalir: UIButton!
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(cargar),for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .gray
        return refreshControl
    }()
    
    var documentos:[DocumentosModel] = [DocumentosModel]()
    var container = [(cell:String,data:[String:Any])]()
    
    struct CellType {
        static let blanco = "CellBlank"
        static let inspeccion = "CellInspeccion"
        static let titulo = "CellTitulo"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cargar()
    }
    
    func initView(){
        table.refreshControl = refreshControl
        btnSalir.addAction {
            self.navigationController?.popViewController(animated: true)
        }
        
        //documentos = Mapper<DocumentosModel>().mapArray(JSONObject: Defaults[.documentos] )!
        initTable()
    }
    
    
    @objc func cargar() {
        
        refreshControl.beginRefreshing()
        
        if documentos.count == 0 {
            showLoading()
        }
        
        DocumentosManager.refrescar { (ok, error) in
            
            self.refreshControl.endRefreshing()
            self.hideLoading()
            
            if ok {
                self.documentos = Mapper<DocumentosModel>().mapArray(JSONObject: Defaults[.documentos] )!
                self.initTable()
                self.table.reloadData()
            }else{
                self.showAlert(error){
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    func initTable(){
        
        container = [(cell:String,data:[String:Any])]()
        container.append((cell: CellType.blanco, data: [:] ))
        
        for item in documentos {
            container.append((cell: CellType.titulo, data: item.toJSON() ))
        }
        
        table.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension DocumentosViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! CellGenerica
        
        if (item.cell == CellType.titulo){
            let doc = DocumentosModel(JSON: item.data)!
            cell.label1.text = doc.nombre
            
            cell.view1.layer.shadowColor = UIColor.gray.cgColor
            cell.img1.layer.opacity = 1
            cell.img2.layer.opacity = 0
            
            if doc.tieneDocumento == 1 {
                cell.img2.layer.opacity = 1
                cell.img1.layer.opacity = 0
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = container[indexPath.row]
        
        if (item.cell == CellType.titulo){
            return 98
        }else if (item.cell == CellType.blanco){
            return 30
        }else{
            return 0
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = container[indexPath.row]
        
        if (item.cell == CellType.titulo){
            let doc = DocumentosModel(JSON: item.data)!
            
            if doc.tieneDocumento == 0 {
                Defaults[.documento_activo] = doc.toJSON()
                let controller = ArchivoViewController.instance(fromAppStoryboard: .Archivo)
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
        }
    }
    
}
