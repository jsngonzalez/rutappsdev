//
//  InspeccionManager.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 20/01/20.
//  Copyright © 2020 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults
import Crashlytics

class InspeccionManager: NSObject {
    
    class func refrescar(callback:@escaping (Bool,String) -> Void = {(ok,error) in} ){
        
        let vehiculo = PlacaModel(JSON: Defaults[.placa_activa] )!
        let url = API.inspecciones + String(vehiculo.idVehiculo)
        Servicios().get(url, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    class func guardar(_ inspecciones:[InspeccionModel],dia:DiasModel, callback:@escaping (Bool,String) -> Void = {(ok,error) in} ){
        
        
        let data = [
            "inspecciones":inspecciones.toJSON(),
            "dia":dia.dia,
            "mes":dia.mes,
            "anio":dia.anio
        ] as [String : Any]
        
        let req = RequestModel(JSON: ["data":data] )!
        
        Servicios().post(API.inspecciones, showLoading:true, parameters: req, callback:{ json in
            
            if validar(json) {
                callback(true, "")
            }else{
                callback(false, (json["response"] as! String) )
            }
            
        })
        
    }
    
    
    
    class func validar(_ json:[String:Any]) -> Bool{
        
        let response = MapResponse.toObject(json, with: InspeccionesModel.self)
        if response.error == 0, let response = response.response {
            Defaults[.inspecciones] = response.toJSON()
            return true
        }else{
            Defaults.remove(.inspecciones)
            return false
        }
        
    }
    
}
