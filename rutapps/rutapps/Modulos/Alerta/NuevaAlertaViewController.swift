//
//  AlertaViewController.swift
//  rutappsusuario
//
//  Created by Jeisson Gonzalez on 11/09/18.
//  Copyright © 2018 Wigilabs. All rights reserved.
//

import UIKit
import PMSuperButton
import SwiftyUserDefaults
import ObjectMapper
import YPImagePicker
import SkeletonView
import Firebase
import FirebaseStorage
import Notifwift
import CoreLocation
import ZVProgressHUD

class NuevaAlertaViewController: UIViewController {
    
    @IBOutlet var lblTituloAlerta: UILabel!
    @IBOutlet var btnAtras: UIButton!
    @IBOutlet var viewTituloAlerta: UIView!
    @IBOutlet var viewContent: SuperView!
    @IBOutlet var viewBg: UIView!
    @IBOutlet var txtMensaje: UITextView!
    @IBOutlet var imgFoto: UIImageView!
    @IBOutlet var btnFoto: PMSuperButton!
    @IBOutlet var viewFoto: UIView!
    @IBOutlet var btnEnviarAlerta: PMSuperButton!
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(cargarAlertas),for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 1, green: 0.6, blue: 0.1411764706, alpha: 1)
        return refreshControl
    }()
    
    
    
    @IBOutlet var table: UITableView!
    
    var container:[AlertaModel]=[]
    var alerta:AlertaModel!
    
    var frameInicioTituloAlerta:CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //sincronizarAlertas(ruta)
        initView()
        agregarBlur()
    }

    
    func agregarBlur(){
        
        self.viewContent.sombra()
        self.view.insertSubview(blureffect(), at: 0)
    }
    
    func initView(){
        
        viewContent.frame = CGRect(x: viewContent.frame.origin.x, y: screen_height, width: viewContent.frame.size.width , height: viewContent.frame.size.height)
        
        frameInicioTituloAlerta = viewTituloAlerta.frame
        
        if Defaults.hasKey(.alertas) {
            container = Mapper<AlertaModel>().mapArray(JSONObject: Defaults[.alertas] )!
        }
        
        activarNotificaciones()
        initHeaderAlerta()
        
        let gestureCerrar = UITapGestureRecognizer(target: self, action:  #selector (self.cerrar))
        viewBg.addGestureRecognizer(gestureCerrar)
        
        txtMensaje.layer.borderWidth = 1
        txtMensaje.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        txtMensaje.layer.cornerRadius = 5
        txtMensaje.delegate = self
        
        txtMensaje.text = .txt_input_adjuntar_archivo
        txtMensaje.textColor = .lightGray
        
        
        viewContent.backgroundColor = .white
        
        if container.count > 0 {
            
            let indexPath = IndexPath(row: 0, section: 0)
            table.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
            
            alerta=container[0]
            setTituloAlerta()
        }
        
        table.addSubview(refreshControl)
        
        viewFoto.frame.origin.y = screen_height
        
    }
    
    func initHeaderAlerta(){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewTituloAlerta.frame = self.frameInicioTituloAlerta
            self.btnAtras.frame = CGRect(x: -35 , y: 8, width: 35, height:35)
        })
    }
    
    func mostrarAtrasAlerta(){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.btnAtras.frame = CGRect(x: 8 , y: 8, width: 35, height: 35)
            self.viewTituloAlerta.frame = CGRect(x: (16 + 35) , y: 8, width: (self.frameInicioTituloAlerta.size.width - (16 + 35) )  , height: self.frameInicioTituloAlerta.size.height)
        })
    }
    
    func activarNotificaciones(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        var frame = viewContent.frame
        if container.count > 4 {
            
            let total = (container.count - 4) * 65
            if (frame.size.height + CGFloat(total)) > screen_height {
                frame.size.height = screen_height - 60
            }else{
                frame.size.height = frame.size.height + CGFloat(total)
            }
            
        }
        
        viewContent.frame = frame
        let heigth = viewContent.frame.size.height + 20
        let destination = screen_height - heigth
        
        UIView.animate(withDuration: 0.2) {
            var frame = self.viewContent.frame
            frame.origin.y = destination
            self.viewContent.frame = frame
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.2) {
            var frame = self.viewContent.frame
            frame.origin.y = self.screen_height
            self.viewContent.frame = frame
        }
        
        super.viewWillDisappear(animated)
    }
    
    func mostrarFoto(){
    
        UIView.animate(withDuration: 0.3) {
            self.viewFoto.layer.opacity = 1
            self.viewFoto.frame.origin.y = self.screen_height - self.viewContent.frame.size.height -  (self.viewFoto.frame.size.height - 10)
        }
    }
    
    @objc func cerrar(_ ok:Bool = false){
        
        view.endEditing(true)
        dismiss(animated: true){
            if ok {
                self.alertStatus("Información enviada exitosamente")
            }
        }
        
    }
    
    @IBAction func cerrarAlerta(_ sender: Any) {
        cerrar()
    }
    
    @IBAction func cerrarKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func setTituloAlerta(){
        
        if let alertaSeleccionada = alerta {
            
            UIView.animate(withDuration: 0.3, animations: {
                            self.viewTituloAlerta.layer.opacity = 0
            }, completion: { finish in
                self.lblTituloAlerta.text = alertaSeleccionada.nombre
                UIView.animate(withDuration: 0.3, animations: {
                    self.viewTituloAlerta.layer.opacity = 1
                })
            })
        }
    }
    
    @IBAction func enviarAlerta(_ sender: Any) {
        
        if let dataAlerta = alerta {
            
            let mensaje = txtMensaje.textColor == .lightGray ? "" : self.txtMensaje.text
            dataAlerta.mensaje = mensaje!
            
            var data = dataAlerta.toJSON()
            data = FirebaseService.agregarMetadatos(data)
            
            ZVProgressHUD.setMaskType(.black)
            ZVProgressHUD.show()
            
            alertasRef.addDocument(data: data) { (error) in
                ZVProgressHUD.dismiss()
                if error != nil {
                    self.showAlert("Hubo un error al intentar enviar la información")
                }else{
                    self.cerrar(true)
                }
            }
        }
        
    }
    
    @IBAction func agregarFoto(_ sender: Any) {
        
        let coloredImage = UIImage(color: #colorLiteral(red: 1, green: 0.6, blue: 0.1411764706, alpha: 1))
        UINavigationBar.appearance().setBackgroundImage(coloredImage, for: UIBarMetrics.default)
        
        let attributes = [NSAttributedString.Key.font : UIFont.init(name: fontConfig.OpenSans.Bold, size: 16)! ]
        UINavigationBar.appearance().titleTextAttributes = attributes // Title fonts
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal) // Bar Button fonts
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) ] // Title color
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // Left. bar buttons
        
        let picker = YPImagePicker()
        
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage ?? "") // Transformed image, can be nil
                print(photo.exifMeta ?? "") // Print exif meta data of original image.
                
                let image = photo.image
                
                let storage = Storage.storage()
                let storageRef = storage.reference()
                
                let archivosRef = storageRef.child("archivos/\(self.ruta_activa.idServicio)/\(UUID().uuidString).jpg")
                let metadata = StorageMetadata()
                metadata.contentType = "image/jpeg"
                
                var imageSend = image.fixedOrientation()
                imageSend = imageSend.resizeImage(600, opaque: true)
                
                
                let data = imageSend.jpegData(compressionQuality: 0.6)
                
                self.btnFoto.showLoader(userInteraction: false)
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.btnEnviarAlerta.layer.opacity = 0
                })
                
                
                let _ = archivosRef.putData(data!, metadata: metadata) { (metadata, error) in
                    
                    self.btnFoto.hideLoader()
                    UIView.animate(withDuration: 0.3, animations: {
                        self.btnEnviarAlerta.layer.opacity = 1
                    })
                    
                    guard let _ = metadata else {
                        // Uh-oh, an error occurred!
                        self.showAlert("Error al intentar subir la imagen")
                        return
                    }
                    
                    archivosRef.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            self.showAlert("Error al intentar subir la imagen")
                            return
                        }
                        
                        self.alerta!.archivo = downloadURL.absoluteString
                        self.btnFoto.hideLoader()
                        self.imgFoto.image = imageSend
                        self.mostrarFoto()
                        UIView.animate(withDuration: 0.4, animations: {
                            self.btnFoto.layer.opacity = 0
                        })
                    }
                }
                
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func borrarFoto(_ sender: Any) {
        
        alerta.archivo = ""
        
        UIView.animate(withDuration: 0.4, animations: {
            self.viewFoto.layer.opacity = 0
            self.viewFoto.frame.origin.y = self.screen_height
        })
        
        UIView.animate(withDuration: 0.4, animations: {
            self.btnFoto.layer.opacity = 1
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            let textFieldHeight=screen_height-keyboardHeight-184
            
            mostrarAtrasAlerta()
            
            UIView.animate(withDuration: 0.6, delay: 0,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.0,
                           options: .curveLinear,
                           animations: {
                            
                            
                            self.viewContent.frame = CGRect(x: self.viewContent.frame.origin.x, y: textFieldHeight, width: self.viewContent.frame.size.width , height: self.viewContent.frame.size.height)
                            
                            
                            if self.alerta!.archivo != "" {
                                var frame = self.viewFoto.frame
                                frame.origin.y = self.screen_height - self.viewContent.frame.size.height - self.viewFoto.frame.origin.y
                                self.viewFoto.frame = frame
                            }
            })
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        initHeaderAlerta()
        
        UIView.animate(withDuration: 0.6, delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.0,
                       options: .curveLinear,
                       animations: {
                        
                        let heigth = self.viewContent.frame.size.height + 20
                        let destination = self.screen_height - heigth
                        
                        var frame = self.viewContent.frame
                        frame.origin.y = destination
                        self.viewContent.frame = frame
                        
                        if self.alerta!.archivo != "" {
                            var frame = self.viewFoto.frame
                            frame.origin.y = self.screen_height - self.viewContent.frame.size.height - self.viewFoto.frame.origin.y
                            self.viewFoto.frame = frame
                        }
        })
    }
    
    @objc func cargarAlertas() {
        
        refreshControl.beginRefreshing()
        
        AlertasManager.refrescar { (ok, error) in
            self.refreshControl.endRefreshing()
            if ok {
                self.container =  Mapper<AlertaModel>().mapArray(JSONObject: Defaults[.alertas] )!
                self.table.reloadData()
            }
        }
        
    }
    
}

extension NuevaAlertaViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtMensaje.textColor == .lightGray {
            txtMensaje.text = nil
            txtMensaje.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtMensaje.text.isEmpty {
            txtMensaje.text = .txt_input_adjuntar_archivo
            txtMensaje.textColor = .lightGray
        }
    }
}



extension NuevaAlertaViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item=container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellMarker", for: indexPath) as! CellGenerica
        
        cell.label1.text = item.nombre
        
        //cell.setAcronimo(item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        alerta=container[indexPath.row]
        print(alerta.toJSONString(prettyPrint: true) ?? "")
        setTituloAlerta()
    }
    
    
}
