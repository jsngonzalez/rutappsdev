//
//  Routing.swift
//  rutapps
//
//  Created by IMAC on 1/08/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import Foundation


class Routing: NSObject {
    
    class func show(_ viewController:UIViewController) {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = viewController
    }
    
    class func popup(_ controller:UIViewController) {
        
        controller.modalTransitionStyle = .crossDissolve
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = .overCurrentContext
        
        if let topController = topViewController() {
            topController.present(controller, animated: false, completion: nil)
        }
        
    }
    
    class func nuevaAlerta() {
        
        let controller = NuevaAlertaViewController.instance(fromAppStoryboard: .NuevaAlerta)
        controller.modalTransitionStyle = .crossDissolve
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = .overCurrentContext
        
        if let topController = topViewController() {
            topController.present(controller, animated: false, completion: nil)
        }
        
    }
    
    class func nuevoEstado() {
        
        let controller = NuevoEstadoViewController.instance(fromAppStoryboard: .NuevoEstado)
        controller.modalTransitionStyle = .crossDissolve
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = .overCurrentContext
        
        if let topController = topViewController() {
            topController.present(controller, animated: false, completion: nil)
        }
        
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    
}
