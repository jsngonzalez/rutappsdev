//
//  View+extension.swift
//  rutapps
//
//  Created by IMAC on 9/03/18.
//  Copyright © 2018 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import Lottie

@IBDesignable
open class SuperView: UIView {
    
    @IBInspectable open var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable open var shadow: Bool = false{
        didSet{
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.layer.shadowOpacity = 0.7
            self.layer.shadowRadius = 5
            self.layer.masksToBounds = false;
        }
    }
    
}

extension UIView {
    func cornerAndShadow(_ color:UIColor, bgColor: UIColor) {
        self.backgroundColor=bgColor
        self.layer.cornerRadius = 10
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 6.0
        self.layer.masksToBounds =  false
        
    }
    
    
    func sombra() {
        
        self.layer.cornerRadius = 20.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 12.0
        self.layer.shadowOpacity = 0.7
        
    }
    
    func loadLocalLottie(_ nombre:String, loop: Bool = true){
        let animationView = AnimationView(name: nombre)
        self.addSubview(animationView)
        
        animationView.frame.size.width = self.frame.size.width
        animationView.frame.size.height = self.frame.size.height
        animationView.frame.origin.x = 0
        animationView.frame.origin.y = 0
        animationView.loopMode = loop ? .loop : .playOnce
        animationView.play()
    }
    
}
