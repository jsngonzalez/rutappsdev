//
//  BuscarActividadViewController.swift
//  rutapps
//
//  Created by Jeisson Gonzalez on 26/11/19.
//  Copyright © 2019 Jeisson Gonzalez. All rights reserved.
//

import UIKit
import ObjectMapper
import Actions

class BuscarActividadViewController: UIViewController {
    
    @IBOutlet var table: UITableView!
    @IBOutlet var viewContainer: SuperView!
    @IBOutlet var txtBuscar: UITextField!
    
    var container : [ActividadModel] = []
    
    struct identificadorCell {
        static let actividad = "CellActividad"
        static let cargando = "CellCargando"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewContainer.bounceIn(from: .bottom)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.viewContainer.frame.origin.y = self.screen_height
        }
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func initView(){
        
        self.view.insertSubview(blureffect(), at: 0)
        
        txtBuscar.add(event: .editingChanged) {
            if self.txtBuscar.text!.count > 1 {
                self.buscar()
            }else{
                self.container = []
                self.table.reloadData()
            }
        }
        
        
        txtBuscar.becomeFirstResponder()
        //loadData()
    }
    
    func buscar(){
        
        self.container = [ActividadModel(JSON: [:])!]
        table.reloadData()
        
        let escapedString = txtBuscar.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let url = API.actividades + "?buscar=" + escapedString
        
        Servicios().get(url, showLoading: false, callback:{ response in
            let res = ActividadModel(JSON:response)!
            if  res.error == 0 {
                self.container = Mapper<ActividadModel>().mapArray(JSONObject: (res.response as! [[String:Any]]) )!
                self.table.reloadData()
                
            }else{
                self.showAlert((res.response as! String))
            }
        })
    }
    
    @IBAction func cerrarmenu(_ sender: Any) {
        cerrar()
    }
    
    
    @IBAction func volver(_ sender: Any) {
        cerrar()
    }
    
    func cerrar(){
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension BuscarActividadViewController:UITableViewDataSource,UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item=container[indexPath.row]
        
        
        if item.nombre != "" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identificadorCell.actividad, for: indexPath) as! CellGenerica
            cell.label1.text = item.nombre
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identificadorCell.cargando, for: indexPath) as! CellGenerica
            return cell
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item=container[indexPath.row]
        if item.nombre != "" {
            return 65
        }else{
            return 60
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return container.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = container[indexPath.row]
        print(item)
        
        let existe = ruta_activa.actividades.filter { (actividad) -> Bool in
            return actividad.idActividad == item.idActividad
        }
        
        if existe.count > 0 {
            showAlert("Esta actividad ya la tienes registrada.")
        }else{
            cerrar()
        }
        
    }
}
